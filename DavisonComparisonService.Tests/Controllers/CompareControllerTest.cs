﻿using System;
using System.Collections.Generic;
using DavisonComparisonService.Controllers;
using DavisonCorpModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DavisonComparisonService.Tests.Controllers
{
    /// <summary>
    /// Tests for the Compare Controller from the Comparison Service
    /// </summary>
    [TestClass]
    public class CompareControllerTest
    {
        /// <summary>
        /// Tests that the GetCheapestProduct method doesn't return a null value
        /// </summary>
        [TestMethod]
        public void GetCheapestproductNotNullTest()
        {
            var controller = new CompareController();
            var result = controller.GetCheapestproduct("111", "Test", "Test", 1, "Test", "1");
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Creates a mock product for testing purposes
        /// </summary>
        [TestMethod]
        public void SetProduct()
        {
            var controller = new CompareController();
            var result = controller.SetProduct("111", "Test", "Test", 1, "Test", "1");
            Assert.IsNotNull(result);
            Assert.AreEqual("111", result.Ean);
            Assert.AreEqual("Test", result.CategoryName);
            Assert.AreEqual("Test", result.Name);
            Assert.AreEqual(1, result.Price);
            Assert.AreEqual("Test", result.ProductSupplier);
            Assert.AreEqual(1, result.Id);
        }

        [TestMethod]
        public void CompareAgainstDavison()
        {
           StoreProduct TestProduct = new StoreProduct
            {
                Ean = "5 102310 300410",
                CategoryName = "Screen Protectors",
                Name = "Rippled Screen Protector",
                Price = 9.99,
                ProductSupplier = "Test Supplier",
                Id = Convert.ToInt32(2),
            };
            var controller = new CompareController();
            var result = controller.CompareAgainstDavison(TestProduct);
            Assert.IsNotNull(result);
            Assert.AreNotEqual(TestProduct.Price,result.Price);
            Assert.IsTrue(result.Price < TestProduct.Price);
        }
    }
}
