﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using DavisonOrdersService.Models;
using Order = DavisonCorpModel.DTO.Order;

namespace DavisonOrdersService.Controllers
{  
    public class OrderController : ApiController
    {
        StoreOrders theOrders = new StoreOrders();

        #region methods for sending orders to suppliers and obtaining returned order details
        /// <summary>
        /// This method sends an order to DavisonCorp as specified by the users requirements and passes back an Order DTO.
        /// </summary>
        /// <param name="AccountName">The name of the account in which the order has been created from</param>
        /// <param name="CardNumber">users card number entered for this order</param>
        /// <param name="ProductName">product name for the item in this order</param>
        /// <param name="ProductEan">product ean for the item in this order</param>
        /// <param name="Price">price for the item in the order</param>
        /// <param name="Quantity">quantity of the product for this order</param>
        /// <returns>An object of type Order containing the order details</returns>
        [HttpGet]
        [Route("GetOrderDetailsFromDavison")]
        public Order GetOrderDetailsFromDavison(string AccountName, string CardNumber, string ProductName, string ProductEan, double Price, int Quantity)
        {
            // Make the order object and populate it with the details
            var theOrder = new Order
            {
                AccountName = AccountName,
                CardNumber = CardNumber,
                Id = theOrders.OrdersInDb.Count() + 1,
                ProductEan = ProductEan,
                ProductId = 0,
                ProductName = ProductName,
                Quantity = Quantity,
                TotalPrice = Price * Quantity,
                When = DateTime.Now.ToLongTimeString()
            };

            // Add the orders information to the orders database.
            theOrders.OrdersInDb.Add(new OrderLog("DavisonCorp", theOrders.OrdersInDb.Count() + 1, ProductName, Quantity, AccountName));
            theOrders.SaveChanges();

            return theOrder; // Return the recently created order object.
        }

        /// <summary>
        /// This method sends an order to Undercutters as specified by the users requirements and passes back an Order DTO.
        /// </summary>
        /// <param name="AccountName">The name of the account in which the order has been created from</param>
        /// <param name="CardNumber">users card number entered for this order</param>
        /// <param name="ProductId">product id for the item in this order</param>
        /// <param name="Quantity">quantity of the product for this order</param>
        /// <returns>An object of type Order containing the order details</returns>
        [HttpGet]
        [Route("GetOrderDetailsFromUndercutters")]
        public Order SendOrderToUndercutters(string AccountName, string CardNumber, int ProductId, int Quantity)
        {
            // Set up connection information for the undercutters service
            var client = new HttpClient { BaseAddress = new System.Uri("http://undercutters.azurewebsites.net/") };
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromMilliseconds(10000);
            
            // Collate the information required to send the order request into an object.
            var data = new { AccountName, CardNumber, ProductId, Quantity };
            
            // Send the order request to the service and obtain the result
            try
            {
                HttpResponseMessage response = client.PostAsJsonAsync("api/Order", data).Result;
                // Providing the result is successful we can convert the received JSON to a Order Object to store the orders details.
                if (response.IsSuccessStatusCode)
                {
                    Task<string> content = response.Content.ReadAsStringAsync();
                    var orderDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<Order>(content.Result); 
                    // Add the orders information to the orders database.
                    theOrders.OrdersInDb.Add(new OrderLog("Undercutters", orderDetails.Id, orderDetails.ProductName, Quantity, AccountName));
                    theOrders.SaveChanges();
                    return orderDetails;
                }
            }
            catch { Debug.WriteLine("Error communicating with Undercutters when trying to send the order."); }

            return new Order(); // If we reach here, something has gone wrong and an empty order is returned.
        }

        /// <summary>
        /// This method sends an order To BazzasBazaar as specified by the users requirements and passes back an Order DTO
        /// </summary>
        /// <param name="AccountName">The name of the account in which the order has been created from</param>
        /// <param name="CardNumber">users card number entered for this order</param>
        /// <param name="ProductId">product id for the item in this order</param>
        /// <param name="Quantity">quantity of the product for this order</param>
        /// <returns>An object of type Order containing the order details</returns>
        [HttpGet]
        [Route("GetOrderDetailsFromBazzas")]
        public Order SendOrderToBazzas(string AccountName, string CardNumber, int ProductId, int Quantity)
        {
            // Create the order with the passed in information
            var bb = new BazzasBazaar.StoreClient();
            var orderCreation = bb.CreateOrder(AccountName, CardNumber, ProductId, Quantity);
            // Collate the returned details in to an object.
            var returnedOrder = new Order
            {
                Id = orderCreation.Id, AccountName = orderCreation.AccountName,
                CardNumber = orderCreation.CardNumber, ProductId = orderCreation.ProductId,
                Quantity = orderCreation.Quantity, When = orderCreation.When.ToLongDateString(),
                ProductName = orderCreation.ProductName, ProductEan = orderCreation.ProductEan,
                TotalPrice = orderCreation.TotalPrice,
            };
            // Add the orders information to the orders database.
            theOrders.OrdersInDb.Add(new OrderLog("BazzasBazaar", returnedOrder.Id, returnedOrder.ProductName, Quantity, AccountName));
            theOrders.SaveChanges();
            
            return returnedOrder;// Return the Order object received back from the BazzasBazaar Web Service
        }

        /// <summary>
        /// This method sends an order to DodgyDealers as specified by the users requirements and passes back an Order DTO.
        /// </summary>
        /// <param name="AccountName">The name of the account in which the order has been created from</param>
        /// <param name="CardNumber">users card number entered for this order</param>
        /// <param name="ProductId">product id for the item in this order</param>
        /// <param name="Quantity">quantity of the product for this order</param>
        /// <returns>An object of type Order containing the order details</returns>
        [HttpGet]
        [Route("GetOrderDetailsFromDodgyDealers")]
        public Order SendOrderToDodgyDealers(string AccountName, string CardNumber, int ProductId, int Quantity)
        {
            // Set up connection information for the DodgyDealers service
            var client = new HttpClient { BaseAddress = new System.Uri("http://dodgydealers.azurewebsites.net/") };
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromMilliseconds(10000);

            // Collate the information required to send the order request into an object.
            var data = new { AccountName, CardNumber, ProductId, Quantity };
            
            // Send the order request to the service and obtain the result
            try
            {
                HttpResponseMessage response = client.PostAsJsonAsync("api/Order", data).Result;
                // Providing the result is successful we can convert the received JSON to a Order Object to store the orders details.
                if (response.IsSuccessStatusCode)
                {
                    Task<string> content = response.Content.ReadAsStringAsync();
                    var orderDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<Order>(content.Result);
                    // Add the orders information to the orders database.
                    theOrders.OrdersInDb.Add(new OrderLog("DodgyDealers", orderDetails.Id, orderDetails.ProductName, Quantity, AccountName));
                    theOrders.SaveChanges();
                    return orderDetails;
                }
            }
            catch { Debug.WriteLine("Error communicating with DodgyDealers when trying to send the order."); }

            // If we reach this point  an exception has been logged, something has gone wrong and an empty order is returned.
            return new Order();
        }
        #endregion

        #region methods for obtaining users orders

        /// <summary>
        /// This method gets all the orders a particular user has placed.
        /// </summary>
        /// <param name="accntName">The account name of the user to query</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetUsersOrders")]
        public IEnumerable<OrderLog> GetUsersOrders(string accntName)
        {
            var orderGetter = theOrders.OrdersInDb.Where(p => p.AccountName == accntName);
            return orderGetter;
        }

        #endregion

        #region methods which are used for cancelling orders

        /// <summary>
        /// This method is used to cancel an order from Undercutters
        /// </summary>
        /// <param name="orderID">The ID of the order to cancel</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("CancelUndercuttersOrder")]
        public string CancelUndercuttersOrder(int orderID)
        {
            // Set up connection information for the undercutters service
            var client = new HttpClient { BaseAddress = new System.Uri("http://undercutters.azurewebsites.net/") };
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromMilliseconds(10000);

            // Send the order delete request to the service and obtain the returned status code
            try
            {
                var response = client.DeleteAsync("api/order?id=" + orderID).Result;    
                if (response.IsSuccessStatusCode)
                {
                    // Delete the order from the Orders DB and return the status code
                    var orderGetter = theOrders.OrdersInDb.Single(o => o.SupplierOrderId == orderID);
                    theOrders.OrdersInDb.Remove(orderGetter);
                    theOrders.SaveChanges();
                    return response.StatusCode.ToString();
                }
            }
            catch { Debug.WriteLine("Error communicating with Undercutters when trying to send the order."); }

            return ""; // If we reach this point something has gone wrong and an empty string is returned.
        }

        /// <summary>
        /// This method is used to cancel DodgyDealers orders
        /// </summary>
        /// <param name="orderID">The ID of the order to cancel</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("CancelDodgyDealersOrder")]
        public string CancelDodgyDealersOrder(int orderID)
        {
            // Set up connection information for the undercutters service
            var client = new HttpClient { BaseAddress = new System.Uri("http://dodgydealers.azurewebsites.net/") };
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromMilliseconds(10000);

            // Send the order delete request to the service and obtain the returned status code
            try
            {
                var response = client.DeleteAsync("api/order?id=" + orderID).Result;
                if (response.IsSuccessStatusCode)
                {
                    // Delete the order from the Orders DB and return the status code
                    var orderGetter = theOrders.OrdersInDb.Single(o => o.SupplierOrderId == orderID);
                    theOrders.OrdersInDb.Remove(orderGetter);
                    theOrders.SaveChanges();
                    return response.StatusCode.ToString();
                }
            }
            catch { Debug.WriteLine("Error communicating with DodgyDealers when trying to send the order."); }

            return ""; // If we reach this point something has gone wrong and an empty string is returned.
        }

        /// <summary>
        /// This method is used to cancel Davison Orders
        /// </summary>
        /// <param name="orderID">The ID of the order to cancel</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("CancelDavisonOrder")]
        public string CancelDavisonOrder(int orderID)
        {
            // Attempt to delete the order from the DB
            try
            {
                // Delete the order from the Orders DB and return the status code
                var orderGetter = theOrders.OrdersInDb.Single(o => o.SupplierOrderId == orderID);
                theOrders.OrdersInDb.Remove(orderGetter);
                theOrders.SaveChanges();
                return "OK";
            }
            catch { Debug.WriteLine("Error deleting DavisonCorp order from DB"); }

            return ""; // If we reach this point something has gone wrong and an empty string is returned.
        }

        /// <summary>
        /// This method is used to cancel Bazzas Orders
        /// </summary>
        /// <param name="orderID">The ID of the order to cancel</param>
        [HttpDelete]
        [Route("CancelBazzasOrder")]
        public string CancelBazzasOrder(int orderID)
        {
            // Send the order delete request to the service and obtain the returned status code
            try
            {
                // Cancel the Bazzas Order
                var bb = new BazzasBazaar.StoreClient();
                var orderDeleter = bb.CancelOrderById(orderID);
                // if successfully cancedlled then remove from DB and return OK
                if (orderDeleter == true)
                {
                    // Delete the order from the Orders DB and return the information
                    var orderGetter = theOrders.OrdersInDb.Single(o => o.SupplierOrderId == orderID);
                    theOrders.OrdersInDb.Remove(orderGetter);
                    theOrders.SaveChanges();
                    return "OK";
                }
            }
            catch { Debug.WriteLine("Error communicating with Bazzas when trying to send the order."); }

            return ""; // If we reach this point something has gone wrong and an empty string is returned.
        }
#endregion

        #region methods which are used for obtaining order details from suppliers.

        /// <summary>
        /// Obtains details for an order from Undercutters
        /// </summary>
        /// <param name="orderID">the id of the order to obtain the details of</param>
        /// <returns>the details for the order id which has been passed in</returns>
        [HttpGet]
        [Route("GetUndercuttersOrderDetails")]
        public Order GetUndercuttersOrderDetails(int orderID)
        {
            // Set up connection information for the undercutters service
            var client = new HttpClient { BaseAddress = new System.Uri("http://undercutters.azurewebsites.net/") };
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromMilliseconds(10000);

            // Send the order request to the service and obtain the result
            try
            {
                HttpResponseMessage response = client.GetAsync("api/Order?id=" + orderID).Result;
                // Providing the result is successful we can convert the received JSON to a Order Object to store the orders details.
                if (response.IsSuccessStatusCode)
                {
                    Task<string> content = response.Content.ReadAsStringAsync();
                    var orderDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<Order>(content.Result);
                    return orderDetails;
                }
            }
            catch { Debug.WriteLine("Error communicating with Undercutters when trying to send the order."); }
            return new Order();
        }

        /// <summary>
        /// Obtains details for an order from Dodgy Dealers
        /// </summary>
        /// <param name="orderID">the id of the order to obtain the details of</param>
        /// <returns>the details for the order id which has been passed in</returns>
        [HttpGet]
        [Route("GetDodgyDealersOrderDetails")]
        public Order GetDodgyDealersOrderDetails(int orderID)
        {
            // Set up connection information for the undercutters service
            var client = new HttpClient { BaseAddress = new System.Uri("http://dodgydealers.azurewebsites.net//") };
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromMilliseconds(10000);

            // Send the order request to the service and obtain the result
            try
            {
                HttpResponseMessage response = client.GetAsync("api/Order?id=" + orderID).Result;
                // Providing the result is successful we can convert the received JSON to a Order Object to store the orders details.
                if (response.IsSuccessStatusCode)
                {
                    Task<string> content = response.Content.ReadAsStringAsync();
                    var orderDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<Order>(content.Result);
                    return orderDetails;
                }
            }
            catch { Debug.WriteLine("Error communicating with DodgyDealers when trying to send the order."); }
            return new Order();
        }

        /// <summary>
        /// Obtains details for an order from DavisonCorp
        /// </summary>
        /// <param name="orderID">the id of the order to obtain the details of</param>
        /// <returns>the details for the order id which has been passed in</returns>
        [HttpGet]
        [Route("GetDavisonCorpOrderDetails")]
        public Order GetDavisonCorpOrderDetails(int orderID)
        {
            var orderDetails = theOrders.OrdersInDb.Single(p => p.SupplierOrderId == orderID);
            var db = new Davison.Model.Context();
            var davisonItem = db.Products.Single(p => p.Name == orderDetails.ProductName);
            var orderObject = new Order
            {
                AccountName = orderDetails.AccountName,
                CardNumber = "",
                Id = orderDetails.SupplierOrderId,
                ProductEan = davisonItem.Ean,
                ProductId = davisonItem.Id,
                ProductName = orderDetails.ProductName,
                Quantity = orderDetails.QuantityOrdered,
                TotalPrice = davisonItem.Price * orderDetails.QuantityOrdered,
                When = DateTime.Now.ToString()
            };
            return orderObject;
        }

        /// <summary>
        /// Obtains details for an order from BazzasBazaar
        /// </summary>
        /// <param name="orderID">the id of the order to obtain the details of</param>
        /// <returns>the details for the order id which has been passed in</returns>
        [HttpGet]
        [Route("GetBazzasBazaarOrderDetails")]
        public Order GetBazzasBazaarOrderDetails(int orderID)
        {
            var bb = new BazzasBazaar.StoreClient();
            var orderDetails = bb.GetOrderById(orderID);
            var orderObject = new Order
            {
                AccountName = orderDetails.AccountName,
                CardNumber = "",
                Id = orderDetails.Id,
                ProductEan = orderDetails.ProductEan,
                ProductId = orderDetails.ProductId,
                ProductName = orderDetails.ProductName,
                Quantity = orderDetails.Quantity,
                TotalPrice = orderDetails.TotalPrice,
                When = orderDetails.When.ToString()
            };
            return orderObject;
        }
#endregion

    }
}
