using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DavisonOrdersService.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class StoreOrders : DbContext
    {
        // Your context has been configured to use a 'StoreOrders' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'DavisonOrdersService.Models.StoreOrders' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'StoreOrders' 
        // connection string in the application configuration file.
        public StoreOrders()
            : base("name=StoreOrders")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
        
        public virtual DbSet<OrderLog> OrdersInDb { get; set; }
    }

    public class OrderLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        
        public string SupplierName { get; set; }

        public int SupplierOrderId { get; set; }
        
        public string ProductName { get; set; }

        public int QuantityOrdered { get; set; }
        
        public string AccountName { get; set; }

        public OrderLog(string SupplierName, int SupplierOrderId, string ProductName, int QuantityOrdered, string AccountName)
        {
            this.SupplierName = SupplierName;
            this.SupplierOrderId = SupplierOrderId;
            this.ProductName = ProductName;
            this.QuantityOrdered = QuantityOrdered;
            this.AccountName = AccountName;
        }

        public OrderLog()
        {
            
        }
    }
}