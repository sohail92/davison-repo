﻿using System;
using System.Collections;
using System.Linq;
using Davison.Model;
using DavisonCorpModel.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Reflection.Emit;
using System.Threading;

namespace DavisonCorpModel.Tests.TestClasses
{
    /// <summary>
    /// This class tests the functionality of the repository.
    /// </summary>
    [TestClass]
    public class RepositoryTest
    {

        // An instance of the repository being tested with the Davison.Model Context passed in.
        ProductRepository myRepo = new ProductRepository(new Davison.Model.Context());

        #region Obtaining Categories
        /// <summary>
        /// This method tests to see if categorys can be obtained from the Davison DB
        /// </summary>
        [TestMethod]
        public void TestDavisonCategoryObtainment()
        {
            // Attempt to obtain a list of categories from the repository
            IEnumerable<Category> categoryList = myRepo.GetDavisonCategories();

            // set count to the number of categories obtained.
            int count = categoryList.Count();

            //If the number of categories obtained is greater then 1 pass the test.
            bool resultIndicator = count > 1;

            Assert.IsTrue(resultIndicator);
        }
        
        /// <summary>
        /// Test to ensure that only categories marked as 'active' as being displayed.
        /// </summary>
        [TestMethod]
        public void TestIfOnlyActiveCategoriesAreBeingDisplayed()
        {
            // Attempt to obtain a list of categories from the repository
            IEnumerable<Category> categoryList = myRepo.GetDavisonCategories();

            // Default resultIndicator bool to true until (if) an inactive category is found
            bool resultIndicator = true;
            
            // Go through each category obtained and if an inactive category is found fail the test
            foreach (var cat in categoryList)
                if (cat.Active != true)
                    resultIndicator = false; // found an inactive category!  

            Assert.IsTrue(resultIndicator);
        }
        #endregion

        #region Obtaining Products from Suppliers [Davison/Undercutters/DodgyDealers/BazzasBazaar]
        /// <summary>
        /// This tests if products can be gathered from the DavisonProductDB
        /// </summary>
        [TestMethod]
        public void TestProductObtainmentFromDavisonDb()
        {
            // Attempt to obtain a list of categories from the repository
            IEnumerable<Category> categoryList = myRepo.GetDavisonCategories();

            int numOfProductsObtained = 0;

            foreach (var cat in categoryList)
            {
                numOfProductsObtained += myRepo.GetDavisonProducts(cat.Name).Count();
            }

            // if the number of products obtained is more then 0 then pass the test
            bool resultIndicator = numOfProductsObtained > 0;

            Assert.IsTrue(resultIndicator);
        }

        /// <summary>
        /// This methods tests if products can be gathered from the Undercutters Web Service.
        /// </summary>
        [TestMethod]
        public void TestProductObtainmentFromUndercutters()
        {
            // Attempt to obtain a list of categories from the repository
            IEnumerable<Category> categoryList = myRepo.GetDavisonCategories();

            int numOfProductsObtained = 0;

            foreach (var cat in categoryList)
            {
                numOfProductsObtained += myRepo.GetUndercuttersProducts(cat.Name).Count();
            }

            // if the number of products obtained is more then 0 then pass the test
            bool resultIndicator = numOfProductsObtained > 0;

            Assert.IsTrue(resultIndicator);
        }

        /// <summary>
        /// This methods tests if products can be gathered from the Undercutters Web Service.
        /// </summary>
        /*[TestMethod]
        public void TestProductObtainmentFromDodgyDealers()
        {
            // Attempt to obtain a list of categories from the repository
            IEnumerable<Category> categoryList = myRepo.GetDavisonCategories();

            int numOfProductsObtained = 0;

            foreach (var cat in categoryList)
            {
                numOfProductsObtained += myRepo.GetDodgyDealersProducts(cat.Name).Count();
            }

            // if the number of products obtained is more then 0 then pass the test
            bool resultIndicator = numOfProductsObtained > 0;

            Assert.IsTrue(resultIndicator);
        }*/

        /// <summary>
        /// This method tests to see if products can successfully be obtained from BazzasBazaar
        /// </summary>
        [TestMethod]
        public void TestProductObtainmentFromBazzas()
        {
            // Obtain categories within the davison store
            IEnumerable<Category> categoryList = myRepo.GetDavisonCategories();

            int numOfProductsObtained = 0;

            foreach (var cat in categoryList)
            {
                numOfProductsObtained += myRepo.GetBazzasProductsByCategory(cat.Name).Count();
            }

            // if the number of products obtained is more then 0 then pass the test
            bool resultIndicator = numOfProductsObtained > 0;

            Assert.IsTrue(resultIndicator);
        }
        #endregion

        #region Product Filtration

        [TestMethod]
        public void TestFilterDavisonByPrice()
        {
            // Set min and max prices for this test
            double minPrice = 5.0;
            double maxPrice = 15.0;

            // obtain categories as its required for the method
            var categoryList = myRepo.GetDavisonCategories().ToList();

            // pass in a known category along with the minprice and maxprice arguments
            var listOfProducts = (myRepo.GetDavisonProductsByPrice(categoryList[0].Name, minPrice, maxPrice));

            // set result bool as false (default)
            bool resultIndicator = true;

            // go through each of the obtained products and check they meet the conditions
            foreach (var product in listOfProducts)
                if((product.Price < minPrice) || (product.Price > maxPrice))
                    resultIndicator = false;

            Assert.IsTrue(resultIndicator);
        }

#endregion

    }
}
