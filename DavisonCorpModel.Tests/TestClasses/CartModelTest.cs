﻿using System;
using System.Collections.Generic;
using System.Linq;
using DavisonCorpModel.Cart;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DavisonCorpModel.Tests.TestClasses
{
    [TestClass]
    public class CartModelTest
    {

        /// <summary>
        /// To test if correct products and quantities are added to the shopping cart.
        /// </summary>
        [TestMethod]
        public void AddProductToCart()
        {
            //get list of test products
            var products = ListOfProducts();

            //create a new cart
            var myCart = new Cart.Cart();

            //add products to cart
            myCart.AddItem(products[0].Name, products[0].Price, products[0].Ean, products[0].ProductSupplier, 1, products[0].Id);
            myCart.AddItem(products[1].Name, products[1].Price, products[1].Ean, products[1].ProductSupplier, 1, products[1].Id);
            myCart.AddItem(products[0].Name, products[0].Price, products[0].Ean, products[0].ProductSupplier, 3, products[0].Id);
            CartItem[] results = myCart.Items.OrderBy(c => c.ProductEan).ToArray();

            //checks
            //2 products in cart
            Assert.AreEqual(2, results.Length);
            //quanitiy of product 1 = 4
            Assert.AreEqual(4, results[0].Quantity);
            //quantity of product 2 = 1
            Assert.AreEqual(1, results[1].Quantity);
        }

        /// <summary>
        /// Tests to see if the total amount (price) in the cart is calculated correctly
        /// </summary>
        [TestMethod]
        public void TotalAmount()
        {
            //get list of test products
            var products = ListOfProducts();

            //create a cart
            var myCart = new Cart.Cart();

            //add items to cart
            myCart.AddItem(products[0].Name, products[0].Price, products[0].Ean, products[0].ProductSupplier, 1, products[0].Id);
            myCart.AddItem(products[2].Name, products[2].Price, products[2].Ean, products[2].ProductSupplier, 2, products[2].Id);

            //get total price
            var TotalPrice = myCart.CalculateTotalCartValue();

            //doubles are approximations not absolute values so a percision value is needed.
            const double precision = 1e-6;

            //check total price is correct
            Assert.AreEqual(7.3d, TotalPrice, precision);
        }

        /// <summary>
        /// Tests if a product can be removed from the shopping cart
        /// </summary>
        [TestMethod]
        public void RemoveProductFromCart()
        {
            //get list of test products
            var products = ListOfProducts();

            //create a cart
            var myCart = new Cart.Cart();

            //add items to cart
            myCart.AddItem(products[0].Name, products[0].Price, products[0].Ean, products[0].ProductSupplier, 1, products[0].Id);
            myCart.AddItem(products[1].Name, products[1].Price, products[1].Ean, products[1].ProductSupplier, 2, products[1].Id);
            myCart.AddItem(products[0].Name, products[0].Price, products[0].Ean, products[0].ProductSupplier, 2, products[0].Id);
            myCart.AddItem(products[2].Name, products[2].Price, products[2].Ean, products[2].ProductSupplier, 5, products[2].Id);

            //remove item from cart
            myCart.RemoveItem(products[0].Name, products[0].Ean, products[0].ProductSupplier);

            //checks
            //check if product[0] no longer exsists
            Assert.AreEqual(0,myCart.Items.Count(c => c.ProductEan == products[0].Ean && c.ProductSupplier == products[0].ProductSupplier));
            //check that there are only 2 lines in the cart
            Assert.AreEqual(2,myCart.Items.Count());
        }

        /// <summary>
        /// Clears the cart of any exsisting items.
        /// </summary>
        [TestMethod]
        public void ClearCart()
        {
            //get list of test products
            var products = ListOfProducts();

            //create a cart
            var myCart = new Cart.Cart();

            //add items to cart
            myCart.AddItem(products[0].Name, products[0].Price, products[0].Ean, products[0].ProductSupplier, 1, products[0].Id);
            myCart.AddItem(products[1].Name, products[1].Price, products[1].Ean, products[1].ProductSupplier, 2, products[1].Id);
            myCart.AddItem(products[0].Name, products[0].Price, products[0].Ean, products[0].ProductSupplier, 2, products[0].Id);
            myCart.AddItem(products[2].Name, products[2].Price, products[2].Ean, products[2].ProductSupplier, 5, products[2].Id);
            myCart.RemoveAllItems();

            //check to see if cart is cleared by checking the count of the items.
            Assert.AreEqual(0,myCart.Items.Count() );
        }

        /// <summary>
        /// Creates a list of products for testing purposes.
        /// </summary>
        /// <returns>A list of test products</returns>
        public List<StoreProduct> ListOfProducts()
        {
            //create products
            var product1 = new StoreProduct
            {
                BrandId = 1,
                BrandName = "BrandName",
                CategoryId = 1,
                CategoryName = "CategoryName",
                Description = "Test Description",
                Ean = "1",
                ExpectedRestock = new DateTime(2016, 1, 1),
                Id = 1,
                InStock = true,
                Name = "TestName",
                Price = 1.1d,
            };
            var product2 = new StoreProduct
            {
                BrandId = 2,
                BrandName = "BrandName2",
                CategoryId = 2,
                CategoryName = "CategoryName2",
                Description = "Test Description2",
                Ean = "2",
                ExpectedRestock = new DateTime(2017, 1, 1),
                Id = 2,
                InStock = true,
                Name = "TestName2",
                Price = 2.1d,
            };
            var product3 = new StoreProduct
            {
                BrandId = 3,
                BrandName = "BrandName3",
                CategoryId = 3,
                CategoryName = "CategoryName3",
                Description = "Test Description3",
                Ean = "3",
                ExpectedRestock = new DateTime(2018, 1, 1),
                Id = 3,
                InStock = true,
                Name = "TestName3",
                Price = 3.1d,
            };

            //create list of products and add products.
            var products = new List<StoreProduct>();
            products.Add(product1);
            products.Add(product2);
            products.Add(product3);

            //return list of products.
            return products;
        }
    }
}
