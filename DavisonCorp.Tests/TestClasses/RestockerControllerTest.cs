﻿using System.Web.Mvc;
using DavisonCorp.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DavisonCorp.Tests.TestClasses
{
    /// <summary>
    /// This class contains methods to test the functionality of the RestockerController
    /// </summary>
    [TestClass]
    public class RestockerControllerTest
    {
        /// <summary>
        /// Tests if deliveries can successfully be viewed
        /// </summary>
        [TestMethod]
        public void ViewDeliverys()
        {
            string eanTest = "123456"; //mock ean

            RestockerController controller = new RestockerController();
            ActionResult result = controller.ViewDeliverys(eanTest);

            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual(eanTest, controller.ViewBag.eanToShow);
        }

        /// <summary>
        /// Tests if the RestockInfo View can be displayed
        /// </summary>
        [TestMethod]
        public void RestockInfoView()
        {
            RestockerController controller = new RestockerController();
            ActionResult result = controller.RestockInfoView();

            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.IsNotNull(controller.ViewBag.productEAN);
        }
    }
}
