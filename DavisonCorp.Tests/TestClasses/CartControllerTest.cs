﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DavisonCorp.Controllers;
using DavisonCorpModel.Cart;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using DavisonCorpModel;

namespace DavisonCorp.Tests.TestClasses
{
    /// <summary>
    /// This class tests the functionality of the Cart Controller
    /// </summary>
    [TestClass]
    public class CartControllerTest
    {
        /// <summary>
        /// test to check if the cart summary returns the correct cart in session.
        /// </summary>
        [TestMethod]
        public void CartSummary()
        {
            //creates a cart and adds products.
            List<StoreProduct> products = ListOfProducts();
            Cart InitialCart = new Cart();
            InitialCart.AddItem(products[0].Name, products[0].Price, products[0].Ean, products[0].ProductSupplier, 1, products[0].Id);
            InitialCart.AddItem(products[1].Name, products[1].Price, products[1].Ean, products[1].ProductSupplier, 2, products[1].Id);
            InitialCart.AddItem(products[0].Name, products[0].Price, products[0].Ean, products[0].ProductSupplier, 2, products[0].Id);

            //sets up the mock controller and mock session.
            var mockControllerContext = new Mock<ControllerContext>();
            var mockSession = new Mock<HttpSessionStateBase>();
            //sets the mock sessions return value.
            mockSession.SetupGet(s => s["Cart"]).Returns(InitialCart);
            //sets the mock controller to return the mock session.
            mockControllerContext.Setup(p => p.HttpContext.Session).Returns(mockSession.Object);

            //creates a controller and sets the controllers context
            var controller = new CartController();
            controller.ControllerContext = mockControllerContext.Object;
            //calls the cart summary method and returns the result. 
            var actual = controller.CartSummary();

            //checks the initial cart and the returned model (Cart) is the same.
            Assert.AreEqual(InitialCart, actual.Model);
        }

        /// <summary>
        /// Tests if products can be removed from the cart
        /// </summary>
        [TestMethod]
        public void RemoveFromCart()
        {
            string returnUrl = "Test URL";
            //creates a cart and adds products.
            List<StoreProduct> products = ListOfProducts();
            Cart InitialCart = new Cart();
            InitialCart.AddItem(products[0].Name, products[0].Price, products[0].Ean, products[0].ProductSupplier, 1, products[0].Id);
            InitialCart.AddItem(products[1].Name, products[1].Price, products[1].Ean, products[1].ProductSupplier, 2, products[1].Id);
            InitialCart.AddItem(products[0].Name, products[0].Price, products[0].Ean, products[0].ProductSupplier, 2, products[0].Id);

            //sets up the mock controller and mock session.
            var mockControllerContext = new Mock<ControllerContext>();
            var mockSession = new Mock<HttpSessionStateBase>();
            //sets the mock sessions return value.
            mockSession.SetupGet(s => s["Cart"]).Returns(InitialCart);
            //sets the mock controller to return the mock session.
            mockControllerContext.Setup(p => p.HttpContext.Session).Returns(mockSession.Object);

            //creates a controller and sets the controllers context
            var controller = new CartController();
            controller.ControllerContext = mockControllerContext.Object;
            //calls the cart summary method and returns the result. 
            var actual = controller.RemoveFromCart(products[0].Name, products[0].Ean, products[0].ProductSupplier,returnUrl);
            var returnedCart = controller.GetCart();

            //check the return url is set to the right place.
            Assert.AreEqual(returnUrl, actual.RouteValues["returnUrl"]);
            //check the redirect is sent to the right action
            Assert.AreEqual("Index", actual.RouteValues["Action"]);
            //check the item has been removed from the cart.
            Assert.AreEqual(1, returnedCart.Items.Count());
        }

        /// <summary>
        /// Creates a list of products for testing purposes
        /// </summary>
        /// <returns>A list of test products</returns>
        public List<StoreProduct> ListOfProducts()
        {
            //create products
            StoreProduct product1 = new StoreProduct
            {
                BrandId = 1,
                BrandName = "BrandName",
                CategoryId = 1,
                CategoryName = "CategoryName",
                Description = "Test Description",
                Ean = "1",
                ExpectedRestock = new DateTime(2016, 1, 1),
                Id = 1,
                InStock = true,
                Name = "TestName",
                Price = 1.1d,
            };
            StoreProduct product2 = new StoreProduct
            {
                BrandId = 2,
                BrandName = "BrandName2",
                CategoryId = 2,
                CategoryName = "CategoryName2",
                Description = "Test Description2",
                Ean = "2",
                ExpectedRestock = new DateTime(2017, 1, 1),
                Id = 2,
                InStock = true,
                Name = "TestName2",
                Price = 2.1d,
            };
            StoreProduct product3 = new StoreProduct
            {
                BrandId = 3,
                BrandName = "BrandName3",
                CategoryId = 3,
                CategoryName = "CategoryName3",
                Description = "Test Description3",
                Ean = "3",
                ExpectedRestock = new DateTime(2018, 1, 1),
                Id = 3,
                InStock = true,
                Name = "TestName3",
                Price = 3.1d,
            };

            //create list of products and add products.
            List<StoreProduct> products = new List<StoreProduct>();
            products.Add(product1);
            products.Add(product2);
            products.Add(product3);

            //return list of products.
            return products;
        }

    }
}
