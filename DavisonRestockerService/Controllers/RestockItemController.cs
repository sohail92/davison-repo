﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Http;
using Davison.Stocker;

namespace DavisonRestockerService.Controllers
{
    public class RestockItemController : ApiController
    {

        /// <summary>
        /// Returns all deliveries for a given EAN
        /// </summary>
        /// <param name="eanNumber">EAN Number to retrieve deliveries for</param>
        /// <returns>IENumerable of all the deliveries for the product</returns>
        [HttpGet]
        [System.Web.Http.Route("GetDeliveriesForProduct")]
        public IHttpActionResult GetDeliveriesForProduct(string eanNumber)
        {
            var stocker = new Stocker();
            IEnumerable<Delivery> deliverys = stocker.QueryDeliveriesByProduct(eanNumber);
            return Ok(deliverys);
        }

        /// <summary>
        /// Returns all delivery request for a given EAN
        /// </summary>
        /// <param name="eanNumber">EAN number to retrieve delivery requests for</param>
        /// <returns>IENumerable of all the delivery requests for the product</returns>
        [HttpGet]
        [System.Web.Http.Route("GetDeliveryRequestsForProduct")]
        public IHttpActionResult GetRequestsForProduct(string eanNumber)
        {
            var stocker = new Stocker();
            IEnumerable<Request> Requests = stocker.QueryRequestsbyProduct(eanNumber);
            return Ok(Requests);
        }

        /// <summary>
        /// Accept a pending delivery and updates information in the DavisonDB as required.
        /// </summary>
        /// <param name="deliveryId">the delivery to accept</param>
        /// <param name="eanNumber">the ean number of the product of which the deliveries should be accepted for</param>
        /// <param name="restockQty">the quantity to update the db by</param>
        /// <returns></returns>
        [HttpGet]
        [System.Web.Http.Route("AcceptDelivery")]
        public IHttpActionResult AcceptDeliveryForProduct(int deliveryId, string eanNumber, int restockQty)
        {
            //Confirm delivery for passed in deliveryID
            var stocker = new Stocker();
            stocker.ConfirmDeliveryForProduct(deliveryId);
            // Update davison store db with delivered quanitity and then update last restock.
            var db = new Davison.Model.Context();
            Davison.Model.Product getEntityFromDatabase = db.Products.FirstOrDefault(d => d.Ean == eanNumber);
            if (getEntityFromDatabase != null)
            {
                getEntityFromDatabase.StockLevel += restockQty; //increment stock level
                getEntityFromDatabase.LastRestock = DateTime.Now; //set restock date
                db.Products.AddOrUpdate(getEntityFromDatabase);
                db.SaveChanges(); //record updated and saved to db
            }
            IEnumerable<Delivery> deliverys = stocker.QueryDeliveriesByProduct(eanNumber);
            return Ok(deliverys);
        }

        /// <summary>
        /// This method obtains products which have hit the minimum threshold for restocking.
        /// </summary>
        /// <returns>A list of products that have hit the minimum restocking threshold</returns>
        [HttpGet]
        [System.Web.Http.Route("GetProductsIfThreshholdHit")]
        public IHttpActionResult DeliverysByThreshhold()
        {
            //get product where eanNumber is equal.
            var stocker = new Stocker();
            var db = new Davison.Model.Context();
            IEnumerable<Davison.Model.Product> products = db.Products;
            // get every product from stocker (by ean) then check if its restockThreshold is greater then the products stock level (in the db).
            var productsToReturn = products.Where(p => stocker.GetProduct(p.Ean).RestockThreshold > p.StockLevel).ToList();
            return Ok(productsToReturn);
        }

        /// <summary>
        /// Used to create a delivery request for a product
        /// </summary>
        /// <param name="productEAN">the ean for the product which the delivery request is being made for</param>
        /// <param name="RestockAmount">the quantity required for the delivery request</param>
        /// <returns></returns>
        [HttpGet]
        [System.Web.Http.Route("CreateDeliveryRequest")]
        public IHttpActionResult CreateDeliveryRequest(string productEAN, int RestockAmount)
        {
            //get product where eanNumber is equal.
            var stocker = new Stocker();
            stocker.CreateRequestForProduct(productEAN, RestockAmount);
            return Ok();
        }

        /// <summary>
        /// Used to create a delivery request for a product
        /// </summary>
        /// <param name="productEAN">the ean for the product which the delivery request is being made for</param>
        /// <param name="RestockAmount">the quantity required for the delivery request</param>
        /// <returns></returns>
        [HttpGet]
        [System.Web.Http.Route("GetThresholdForProduct")]
        public IHttpActionResult GetThresholdForProduct(string productEAN)
        {
            //get product where eanNumber is equal.
            var stocker = new Stocker();
            int threshhold = stocker.GetProduct(productEAN).RestockThreshold;
            return Ok(threshhold);
        }

    }
}
