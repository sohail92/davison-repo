﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DavisonCorp.Startup))]
namespace DavisonCorp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
