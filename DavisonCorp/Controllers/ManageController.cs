﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using DavisonCorpModel.DTO;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using DavisonCorp.Models;
using System.Net.Http;

namespace DavisonCorp.Controllers
{
    /// <summary>
    /// Auto-generated ManageController
    /// Code has been written here for managing a specific users orders (obtaining details and cancelling)
    /// </summary>
    [Authorize]
    public class ManageController : Controller
    {
        #region Auto-generated code from Identity
        public ManageController()
        {
        }

        public ManageController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: /Manage/Index
        public async Task<ActionResult> Index(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.SetTwoFactorSuccess ? "Your two-factor authentication provider has been set."
                : message == ManageMessageId.Error ? "An error has occurred."
                : message == ManageMessageId.AddPhoneSuccess ? "Your phone number was added."
                : message == ManageMessageId.RemovePhoneSuccess ? "Your phone number was removed."
                : "";

            var model = new IndexViewModel
            {
                HasPassword = HasPassword(),
                PhoneNumber = await UserManager.GetPhoneNumberAsync(User.Identity.GetUserId()),
                TwoFactor = await UserManager.GetTwoFactorEnabledAsync(User.Identity.GetUserId()),
                Logins = await UserManager.GetLoginsAsync(User.Identity.GetUserId()),
                BrowserRemembered = await AuthenticationManager.TwoFactorBrowserRememberedAsync(User.Identity.GetUserId())
            };
            return View(model);
        }

        // POST: /Manage/EnableTwoFactorAuthentication
        [HttpPost]
        public async Task<ActionResult> EnableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), true);
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInAsync(user, isPersistent: false);
            }
            return RedirectToAction("Index", "Manage");
        }

        // POST: /Manage/DisableTwoFactorAuthentication
        [HttpPost]
        public async Task<ActionResult> DisableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), false);
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInAsync(user, isPersistent: false);
            }
            return RedirectToAction("Index", "Manage");
        }

        // GET: /Manage/ChangePassword
        public ActionResult ChangePassword()
        {
            return View();
        }

        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInAsync(user, isPersistent: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
            }
            AddErrors(result);
            return View(model);
        }

        // GET: /Manage/SetPassword
        public ActionResult SetPassword()
        {
            return View();
        }

        // POST: /Manage/SetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetPassword(SetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
                if (result.Succeeded)
                {
                    var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                    if (user != null)
                    {
                        await SignInAsync(user, isPersistent: false);
                    }
                    return RedirectToAction("Index", new { Message = ManageMessageId.SetPasswordSuccess });
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // GET: /Manage/ManageLogins
        public async Task<ActionResult> ManageLogins(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return View("Error");
            }
            var userLogins = await UserManager.GetLoginsAsync(User.Identity.GetUserId());
            var otherLogins = AuthenticationManager.GetExternalAuthenticationTypes().Where(auth => userLogins.All(ul => auth.AuthenticationType != ul.LoginProvider)).ToList();
            ViewBag.ShowRemoveButton = user.PasswordHash != null || userLogins.Count > 1;
            return View(new ManageLoginsViewModel
            {
                CurrentLogins = userLogins,
                OtherLogins = otherLogins
            });
        }

        // POST: /Manage/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new AccountController.ChallengeResult(provider, Url.Action("LinkLoginCallback", "Manage"), User.Identity.GetUserId());
        }

        // GET: /Manage/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
            if (loginInfo == null)
            {
                return RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
            }
            var result = await UserManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login);
            return result.Succeeded ? RedirectToAction("ManageLogins") : RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
        }
        #endregion

        #region Code which involves obtaining order details for a specific order which a user has placed

        public ActionResult ViewOrders()
        {
            // Object created to store the obtained order information from the service
            var retrievedOrderInformation = new List<OrderLog>();
            // Set up connection to the web service.
            var client = new HttpClient { BaseAddress = new System.Uri("http://localhost:49623/") };
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromMilliseconds(10000);
            // Attempt to send the order to BazzasBazaar and retrieve the returned data
            try
            {
                var accountName = User.Identity.Name;
                var response = client.GetAsync("GetUsersOrders?accntName=" + accountName).Result;
                // if successful return the data, otherwise log the bad response
                if (response.IsSuccessStatusCode)
                {
                    retrievedOrderInformation = response.Content.ReadAsAsync<List<OrderLog>>().Result;
                    return View(retrievedOrderInformation);
                }
                else Debug.WriteLine(DateTime.Now + "Received a bad response from the Orders web service. Response: " + response.StatusCode);
            }
            catch (Exception ex) { Debug.WriteLine("Error getting response from Orders web service: " + ex); }
            // if we get here something has gone wrong
            return View();
        }

        public ActionResult OrderDetails(string idNumber, string supplier)
        {
            if (supplier == "Undercutters")
                return ObtainUndercuttersOrderDetails(idNumber);
            else if (supplier == "DodgyDealers")
                return ObtainDodgyDealersOrderDetails(idNumber);
            else if (supplier == "BazzasBazaar")
                return ObtainBazzasBazaarOrderDetails(idNumber);
            else if (supplier == "DavisonCorp")
                return ObtainDavisonCorpOrderDetails(idNumber);

            // If we get here something has gone wrong.
            return View();
        }

        private ActionResult ObtainDavisonCorpOrderDetails(string idNumber)
        {
            // Object created to store the obtained order information from the service
            var retrievedOrderInformation = new DavisonCorpModel.DTO.Order();
            // Set up connection to the web service.
            var client = new HttpClient { BaseAddress = new System.Uri("http://localhost:49623/") };
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromMilliseconds(10000);
            // Attempt to send the order to BazzasBazaar and retrieve the returned data
            try
            {
                var response = client.GetAsync("GetDavisonCorpOrderDetails?orderID=" + idNumber).Result;
                // if successful return the data, otherwise log the bad response
                if (response.IsSuccessStatusCode)
                {
                    retrievedOrderInformation = response.Content.ReadAsAsync<DavisonCorpModel.DTO.Order>().Result;
                    return View(retrievedOrderInformation);
                }
                else Debug.WriteLine(DateTime.Now + "Received a bad response from the Orders web service. Response: " + response.StatusCode);
            }
            catch (Exception ex) { Debug.WriteLine("Error getting response from Orders web service: " + ex); }
            return View();
        }

        private ActionResult ObtainBazzasBazaarOrderDetails(string idNumber)
        {
            // Object created to store the obtained order information from the service
            var retrievedOrderInformation = new DavisonCorpModel.DTO.Order();
            // Set up connection to the web service.
            var client = new HttpClient { BaseAddress = new System.Uri("http://localhost:49623/") };
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromMilliseconds(10000);
            // Attempt to retrieve the order details
            try
            {
                var response = client.GetAsync("GetBazzasBazaarOrderDetails?orderID=" + idNumber).Result;
                // if successful return the data, otherwise log the bad response
                if (response.IsSuccessStatusCode)
                {
                    retrievedOrderInformation = response.Content.ReadAsAsync<DavisonCorpModel.DTO.Order>().Result;
                    return View(retrievedOrderInformation);
                }
                else Debug.WriteLine(DateTime.Now + "Received a bad response from the Orders web service. Response: " + response.StatusCode);
            }
            catch (Exception ex) { Debug.WriteLine("Error getting response from Orders web service: " + ex); }
            return View();
        }

        private ActionResult ObtainDodgyDealersOrderDetails(string idNumber)
        {
            // Object created to store the obtained order information from the service
            var retrievedOrderInformation = new DavisonCorpModel.DTO.Order();
            // Set up connection to the web service.
            var client = new HttpClient { BaseAddress = new System.Uri("http://localhost:49623/") };
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromMilliseconds(10000);
            // Attempt to retrieve the order details
            try
            {
                var response = client.GetAsync("GetDodgyDealersOrderDetails?orderID=" + idNumber).Result;
                // if successful return the data, otherwise log the bad response
                if (response.IsSuccessStatusCode)
                {
                    retrievedOrderInformation = response.Content.ReadAsAsync<DavisonCorpModel.DTO.Order>().Result;
                    return View(retrievedOrderInformation);
                }
                else Debug.WriteLine(DateTime.Now + "Received a bad response from the Orders web service. Response: " + response.StatusCode);
            }
            catch (Exception ex) { Debug.WriteLine("Error getting response from Orders web service: " + ex); }
            return View();
        }

        public System.Web.Mvc.ViewResult ObtainUndercuttersOrderDetails(string idNumber)
        {
            // Object created to store the obtained order information from the service
            var retrievedOrderInformation = new DavisonCorpModel.DTO.Order();
            // Set up connection to the web service.
            var client = new HttpClient { BaseAddress = new System.Uri("http://localhost:49623/") };
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromMilliseconds(10000);
            // Attempt to retrieve the order details
            try
            {
                var response = client.GetAsync("GetUndercuttersOrderDetails?orderID=" + idNumber).Result;
                // if successful return the data, otherwise log the bad response
                if (response.IsSuccessStatusCode)
                {
                    retrievedOrderInformation = response.Content.ReadAsAsync<DavisonCorpModel.DTO.Order>().Result;
                    return View(retrievedOrderInformation);
                }
                else Debug.WriteLine(DateTime.Now + "Received a bad response from the Orders web service. Response: " + response.StatusCode);
            }
            catch (Exception ex) { Debug.WriteLine("Error getting response from Orders web service: " + ex); }
            return View();
        }
        #endregion

        #region Code which involves cancelling a users orders

        /// <summary>
        /// Method called when a user attempts to cancel one of their orders
        /// Depending on the supplier the appropriate method for order cancellation is selected
        /// </summary>
        /// <param name="idNumber">the id of the order</param>
        /// <param name="supplier">the supplier of the order</param>
        /// <returns></returns>
        public ActionResult CancelOrder(string idNumber, string supplier)
        {
            // Convert the orderID to an int
            int orderID = Convert.ToInt32(idNumber);
            string returnedInfo = "";

            if (supplier == "Undercutters")
                returnedInfo = CancelUndercuttersOrder(orderID);
            else if (supplier == "DodgyDealers")
                returnedInfo = CancelDodgyDealersOrder(orderID);
            else if (supplier == "BazzasBazaar")
                returnedInfo = CancelBazzasOrder(orderID);
            else if (supplier == "DavisonCorp")
                returnedInfo = CancelDavisonOrder(orderID);

            ViewData.Add("cancellationInfo", returnedInfo);
            return View();
        }

        /// <summary>
        /// Method called to cancel a DavisonCorp order
        /// </summary>
        /// <param name="idNumber">the id of the order to cancel</param>
        private string CancelDavisonOrder(int idNumber)
        {
            // Set up connection to the web service.
            var client = new HttpClient { BaseAddress = new System.Uri("http://localhost:49623/") };
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromMilliseconds(10000);
            // Attempt to cancel the DavisonCorp order and retrieve the returned data
            try
            {
                var response = client.DeleteAsync("CancelDavisonOrder?orderID=" + idNumber).Result;

                // if successful return the data, otherwise log the bad response
                if (response.IsSuccessStatusCode)
                {
                    string retrievedInformation = response.Content.ReadAsAsync<string>().Result;
                    return retrievedInformation;
                }
                else Debug.WriteLine(DateTime.Now + "Received a bad response from the Orders web service. Response: " + response.StatusCode);
            }
            catch (Exception ex) { Debug.WriteLine("Error getting response from Orders web service: " + ex); }
            return "Unable to delete order. Please contact support for further assistance";
        }

        /// <summary>
        /// Method called to cancel an order from Bazzas Bazaar
        /// </summary>
        /// <param name="idNumber">the id of the order to cancel</param>
        private string CancelBazzasOrder(int idNumber)
        {
            // Set up connection to the web service.
            var client = new HttpClient { BaseAddress = new System.Uri("http://localhost:49623/") };
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromMilliseconds(10000);
            // Attempt to cancel the BazzasBazaar order and retrieve the returned data
            try
            {
                var response = client.DeleteAsync("CancelBazzasOrder?orderID=" + idNumber).Result;

                // if successful return the data, otherwise log the bad response
                if (response.IsSuccessStatusCode)
                {
                    string retrievedInformation = response.Content.ReadAsAsync<string>().Result;
                    return retrievedInformation;
                }
                else Debug.WriteLine(DateTime.Now + "Received a bad response from the Orders web service. Response: " + response.StatusCode);
            }
            catch (Exception ex) { Debug.WriteLine("Error getting response from Orders web service: " + ex); }
            return "Unable to delete order. Please contact support for further assistance";
        }

        /// <summary>
        /// Method to cancel an order from Dodgy Dealers
        /// </summary>
        /// <param name="idNumber">Id of the order to cancel</param>
        /// <returns></returns>
        private string CancelDodgyDealersOrder(int idNumber)
        {
            // Set up connection to the web service.
            var client = new HttpClient { BaseAddress = new System.Uri("http://localhost:49623/") };
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromMilliseconds(10000);
            // Attempt to cancel the dodgydealers order and retrieve the returned data
            try
            {
                var response = client.DeleteAsync("CancelDodgyDealersOrder?orderID=" + idNumber).Result;

                // if successful return the data, otherwise log the bad response
                if (response.IsSuccessStatusCode)
                {
                    string retrievedInformation = response.Content.ReadAsAsync<string>().Result;
                    return retrievedInformation;
                }
                else Debug.WriteLine(DateTime.Now + "Received a bad response from the Orders web service. Response: " + response.StatusCode);
            }
            catch (Exception ex) { Debug.WriteLine("Error getting response from Orders web service: " + ex); }
            return "Unable to delete order. Please contact support for further assistance";
        }

        /// <summary>
        /// Method to cancel an order from Undercutters
        /// </summary>
        /// <param name="idNumber">the id of the order to cancel</param>
        /// <returns></returns>
        private string CancelUndercuttersOrder(int idNumber)
        {
            // Set up connection to the web service.
            var client = new HttpClient { BaseAddress = new System.Uri("http://localhost:49623/") };
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromMilliseconds(10000);
            // Attempt to cancel the undercutters order and retrieve the returned data
            try
            {
                var response = client.DeleteAsync("CancelUndercuttersOrder?orderID=" + idNumber).Result;

                // if successful return the data, otherwise log the bad response
                if (response.IsSuccessStatusCode)
                {
                    string retrievedInformation = response.Content.ReadAsAsync<string>().Result;
                    return retrievedInformation;
                }
                else Debug.WriteLine(DateTime.Now + "Received a bad response from the Orders web service. Response: " + response.StatusCode);
            }
            catch (Exception ex) { Debug.WriteLine("Error getting response from Orders web service: " + ex); }
            return "Unable to delete order. Please contact support for further assistance";
        }
        #endregion

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie, DefaultAuthenticationTypes.TwoFactorCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = isPersistent }, await user.GenerateUserIdentityAsync(UserManager));
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        private bool HasPhoneNumber()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PhoneNumber != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

#endregion
    }
}