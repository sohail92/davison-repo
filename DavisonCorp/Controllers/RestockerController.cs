﻿using System;
using System.Linq;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Web.Mvc;
using DavisonCorpModel.Restocking;

namespace DavisonCorp.Controllers
{
    /// <summary>
    /// Class for controlling restocking
    /// Functionality limited to users within the admin role only.
    /// </summary>
    [Authorize(Roles = "Admin")]
    public class RestockerController : Controller
    {

        /// <summary>
        /// Take user to the ViewDeliverys page
        /// </summary>
        /// <param name="ean">Used to set the ViewBag - which is then passed to later partial views</param>
        /// <returns></returns>
        public ActionResult ViewDeliverys(string ean)
        {
            ViewBag.eanToShow = ean;
            return View();
        }

        // GET: AcceptDelivery
        public ActionResult AcceptDelivery(int deliveryId, string productEan, string restockAmount)
        {
            var client = new HttpClient { BaseAddress = new System.Uri("http://localhost:42636/") };
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromMilliseconds(20000);
            try
            {
                HttpResponseMessage response =
                    client.GetAsync("AcceptDelivery?deliveryId=" + deliveryId + "&eanNumber=" + productEan +
                                    "&restockQty=" + restockAmount).Result;
                // if successful display the data, otherwise log the bad response
                if (response.IsSuccessStatusCode)
                {
                    IEnumerable<Delivery> Deliverys = response.Content.ReadAsAsync<IEnumerable<Delivery>>().Result;
                    ViewBag.eanToShow = productEan;
                    TempData["alertMessage"] = "The delivery was accepted sucessfully.";
                    return View("ViewDeliverys", Deliverys);
                }
                else
                {
                    TempData["alertMessage"] = "The delivery was not accepted - please try again later.";
                    Debug.WriteLine("Received a bad response from the web service.");
                }
            }
            catch
            {
                TempData["alertMessage"] = "Could not connect to re-stocker - please try again later";
                Debug.WriteLine("Error getting response from Restocker Service.");
            }
            ViewBag.eanToShow = productEan;
            return View("ViewDeliverys", null);
        }

        /// <summary>
        /// Choice for user to schedule a delivery or view deliveries in progress for a specific item (ean)
        /// </summary>
        public ActionResult RestockInfoButtonChoice(string productEAN, string restockAmount, string Restock, string ViewD)
        {
            var db = new Davison.Model.Context();
            List<Davison.Model.Product> products = db.Products.ToList();
            if (ViewD != null)
            { // Take user to view deliveries page
                ViewBag.eanToShow = productEAN;
                return View("ViewDeliverys");
            }
            else
            {
                if ((restockAmount != "") && (restockAmount != "0")) //ensure valid restock request
                {
                    var client = new HttpClient {BaseAddress = new System.Uri("http://localhost:42636/")};
                    client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
                    client.Timeout = TimeSpan.FromMilliseconds(20000);
                    try
                    {
                        HttpResponseMessage response =
                            client.GetAsync("CreateDeliveryRequest?productEAN=" + productEAN + "&RestockAmount=" +
                                            restockAmount).Result;
                        // if successful display the data, otherwise log the bad response
                        if (response.IsSuccessStatusCode)
                        {
                            ViewBag.productEAN = new SelectList(products, "Ean", "Name");
                            ModelState.Clear(); // clears form fields
                            TempData["alertMessage"] = "The delivery request was successfully created.";
                            return View("RestockInfoView", products);
                        }
                        else
                        {
                            TempData["alertMessage"] =
                                "There has been a problem with the re-stocker please try again later.";
                            Debug.WriteLine("Received a bad response from the web service.");
                        }
                    }
                    catch
                    {
                        TempData["alertMessage"] =
                            "There has been a problem connecting to the re-stocker please try again later.";
                        Debug.WriteLine("Error getting response from Restocker Service.");
                    }
                }
                else
                {
                    ModelState.Clear(); // clears form fields
                    ViewBag.productEAN = new SelectList(products, "Ean", "Name");
                    TempData["alertMessage"] = "Please enter a valid quantity to re-stock";
                    return View("RestockInfoView", products);
                }
            }
            ModelState.Clear(); // clears form fields
            ViewBag.productEAN = new SelectList(products, "Ean", "Name");
            return View("RestockInfoView", products);
        }

        /// <summary>
        /// Returns the page showing a drop down list of all possible products for restocking purposes
        /// </summary>
        /// <returns>view showing restocking options</returns>
        public ActionResult RestockInfoView()
        {
            var db = new Davison.Model.Context();
            List<Davison.Model.Product> products = db.Products.ToList();
            ViewBag.productEAN = new SelectList(products, "Ean", "Name");
            return View(products);
        }

        /// <summary>
        /// Obtains page displaying products whos stock level is below their minimum stock threshold
        /// </summary>
        /// <returns>partial view showing products which exceed their specific minimum threshold</returns>
        public ActionResult ProductsExceedingThreshhold()
        {
            var client = new HttpClient { BaseAddress = new System.Uri("http://localhost:42636/") };
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromMilliseconds(20000);
            try
            {
                HttpResponseMessage response = client.GetAsync("GetProductsIfThreshholdHit").Result;
                // if successful display the data, otherwise log the bad response
                if (response.IsSuccessStatusCode)
                {
                    var Deliverys = response.Content.ReadAsAsync<IEnumerable<Davison.Model.Product>>().Result;
                    ViewBag.Threshholds = new int[50];
                    int counter = 0;
                    foreach (Davison.Model.Product p in Deliverys)
                    {

                        HttpResponseMessage response2 = client.GetAsync("GetThresholdForProduct?productEAN="+p.Ean).Result;
                        if (response2.IsSuccessStatusCode)
                        {
                           var thresh = response2.Content.ReadAsAsync<int>().Result;
                           ViewBag.Threshholds[counter]= thresh;
                            counter++;
                        }
                    }
                    return View("_HitRestockThreshViewPartial", Deliverys);
                }
                else
                    Debug.WriteLine("Received a bad response from the web service.");
            }
            catch (Exception ex) { Debug.WriteLine("Error getting response from Restocker Service: " + ex); }
            return View("_HitRestockThreshViewPartial", null);
        }

        /// <summary>
        /// Obtains a partial view of deliveries which have received for a particular product but awaiting acceptance
        /// </summary>
        /// <param name="ean">the ean of the product which has a delivery waiting</param>
        /// <returns></returns>
        public PartialViewResult DeliveriesAwaitingConfirm(string ean)
        {
            var client = new HttpClient { BaseAddress = new System.Uri("http://localhost:42636/") };
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromMilliseconds(20000);
            try
            {
                HttpResponseMessage response = client.GetAsync("GetDeliveriesForProduct?eanNumber=" + ean).Result;
                // if successful display the data, otherwise log the bad response
                if (response.IsSuccessStatusCode)
                {
                    var Deliverys = response.Content.ReadAsAsync<IEnumerable<Delivery>>().Result;
                    return PartialView("_DeliveriesPartial", Deliverys);
                }
                else
                    Debug.WriteLine("Received a bad response from the web service.");
            }
            catch
            {
                Debug.WriteLine("Error getting response from Restocker Service."); 
                
            }
            return PartialView("_DeliveriesPartial", null);
        }

        /// <summary>
        /// Obtains requested deliveries for a given ean
        /// </summary>
        /// <param name="ean">the products ean for which requested deliveries should be obtained for</param>
        /// <returns></returns>
        public PartialViewResult DeliveriesRequested(string ean)
        {
            var client = new HttpClient { BaseAddress = new System.Uri("http://localhost:42636/") };
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromMilliseconds(20000);
            try
            {
                HttpResponseMessage response = client.GetAsync("GetDeliveryRequestsForProduct?eanNumber=" + ean).Result;
                // if successful display the data, otherwise log the bad response
                if (response.IsSuccessStatusCode)
                {
                    var Requests = response.Content.ReadAsAsync<IEnumerable<Request>>().Result;
                    return PartialView("_RequestsPartial", Requests);
                }
                else
                    Debug.WriteLine("Received a bad response from the web service.");
            }
            catch
            {
                Debug.WriteLine("Error getting response from Restocker Service: ");
            }
            return PartialView("_RequestsPartial", null);
        }

    }
}