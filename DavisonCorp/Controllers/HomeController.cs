﻿using System;
using System.Diagnostics;
using System.Web.Mvc;
using DavisonCorp.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DavisonCorp.Controllers
{
    /// <summary>
    /// Auto-generated HomeController from VS
    /// Additional functionality has been added for making default user roles and an admin account.
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Takes user to the index page.
        /// Prerequisites involve checking if admin + user roles are available
        /// If not creating them, along with the default administrator account.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            // Code block run to ensure user roles and default admin has been set.
            using (var context = new ApplicationDbContext())
            {
                // Initialise variables for accessing roles and users.
                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                // Checks to see if starting roles (Admin and User) are available.
                EnsureAdminAndUserRolesAreAvailable(roleManager);
                // Creates an admin account using default settings (if it doesn't already exist).
                CreateInitialAdminAccount(userManager);
                // Saves any changes to the database.
                context.SaveChanges();
            }
            return View();
        }

        /// <summary>
        /// This method checks to see if an administrator account is available.
        /// If there is no admin account then one is created.
        /// </summary>
        /// <param name="userManager">The UserManager object which checks is the user exists.</param>
        private void CreateInitialAdminAccount(UserManager<ApplicationUser> userManager)
        {
            try
            {
                // Check if admin@davisonstore.com account already exists.
                if (userManager.FindByEmail("admin@davisonstore.com") == null)
                {
                    // Create admin account
                    var user = new ApplicationUser()
                    {
                        Email = "admin@davisonstore.com",
                        UserName = "admin@davisonstore.com"
                    };
                    // Set the admin password and then create the user.
                    const string password = "myPassword1!";
                    IdentityResult result = userManager.Create(user, password);
                    // Add the admin user to both the admin and user roles.
                    userManager.AddToRole(user.Id, "Admin");
                    userManager.AddToRole(user.Id, "User");
                }
            }
            catch (Exception e) {Debug.WriteLine("There was an error creating the initial admin account, exception: " + e);}
        }

        /// <summary>
        /// This method checks to see the admin and user roles have been created. If not it creates them.
        /// </summary>
        /// <param name="roleManager">The RoleManager object used here to create user roles.</param>
        private void EnsureAdminAndUserRolesAreAvailable(RoleManager<IdentityRole> roleManager)
        {
            // Check If admin and user roles exist, if not create them.
            try
            {
                var adminRole = roleManager.RoleExists("Admin");
                var userRole = roleManager.RoleExists("User");
                if (adminRole == false)
                    roleManager.Create(new IdentityRole("Admin"));
                if (userRole == false)
                    roleManager.Create(new IdentityRole("User"));
            }
            catch (Exception ex) { Debug.WriteLine("Error creating default roles (Admin+User), exception text = " + ex); }
        }

        /// <summary>
        /// Takes the user to the about page
        /// </summary>
        /// <returns></returns>
        public ActionResult About()
        {
            ViewBag.Message = "The Davison Corporation Web Store which sells products from a variety of suppliers";
            return View();
        }

        /// <summary>
        /// Takes the user to the contact page
        /// </summary>
        /// <returns></returns>
        public ActionResult Contact()
        {
            ViewBag.Message = "We have no contact details at the moment, sorry!";
            return View();
        }
    }
}