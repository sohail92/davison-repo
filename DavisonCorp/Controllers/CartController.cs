﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Web.Mvc;
using DavisonCorpModel;
using RedirectToRouteResult = System.Web.Mvc.RedirectToRouteResult;
using DavisonCorpModel.DTO;
using DavisonCorpModel.Cart;

namespace DavisonCorp.Controllers
{
    /// <summary>
    /// The controller class for the stores shopping cart.
    /// </summary>
    public class CartController : Controller
    {

        /// <summary>
        /// Sets up the Cart Return View Model for the current session
        /// </summary>
        /// <param name="returnUrl">the page to take the user back to</param>
        /// <returns>A view showing information for the current sessions cart.</returns>
        public ViewResult Index(string returnUrl)
        {
            return View(new CartReturnViewModel
            {
                cart = GetCart(),
                returnUrl = returnUrl
            });
        }

        /// <summary>
        /// Gets current cart from session
        /// </summary>
        /// <returns>Gets the current cart object for the session</returns>
        public Cart GetCart()
        {
            // Try obtaining the current sessions cart, and if none available then create a new one.
            var cart = (Cart) Session["Cart"];
            if (cart == null)
            {
                cart = new Cart();
                Session["Cart"] = cart;
            }
            return cart;
        }

        /// <summary>
        /// Used to get a current cart summary to display as a widget (top of screen)
        /// </summary>
        /// <returns>A partial view showing a summary of the current sessions cart.</returns>
        public PartialViewResult CartSummary()
        {
            Cart cart = GetCart(); // Obtains the current cart
            return PartialView(cart); //displays the cart widget as a partial view
        }

        /// <summary>
        /// This method simply takes in a product and adds it to the cart.
        /// </summary>
        /// <param name="myProduct">The product to add to the cart</param>
        /// <param name="returnUrl">The initial page where the item was selected from</param>
        /// <returns>Takes the user to the cart.</returns>
        public ActionResult AddToCart(string productName, double productPrice, string productEan, string productSupplier, string productCategory, string compare, string returnURL, string productId, int qty)
        {
            // Obtains product data passed in from the CheaperItem (comparison) view.
            returnURL = Server.UrlDecode(returnURL);
            productEan = Server.UrlDecode(productEan);
            productCategory = Server.UrlDecode(productCategory);
            productName = Server.UrlDecode(productName);
            productId = Server.UrlDecode(productId);

            // Set the redirectString as required.
            string redirectString;
            if (returnURL == null)
                redirectString = Request.UrlReferrer.ToString();
            else
                redirectString = returnURL;
            
            // If a comparison is required query the comparison service to see which cheaper items are available.
            if (compare == "Yes")
            {
                StoreProduct CheapestItem = compareItemForCart(productName, productPrice, productEan, productSupplier, productCategory, productId);
                if (CheapestItem != null && CheapestItem.Price < productPrice)
                {
                    ViewBag.RedirectURL = redirectString;
                    // Add the cheapest item obtained and the users selected item to the list to pass to the CheaperItem view
                    var items = new List<StoreProduct>();
                    items.Add(CheapestItem);
                    var initialProduct = new StoreProduct()
                    {
                        Name = productName,
                        Price = productPrice,
                        Ean = productEan,
                        ProductSupplier = productSupplier,
                        CategoryName = productCategory,
                        Id = Convert.ToInt16(productId)
                    };
                    items.Add(initialProduct);
                    return View("CheaperItem", items);
                }
                else
                {
                    GetCart().AddItem(productName, productPrice, productEan, productSupplier, qty, Convert.ToInt16(productId));
                }
            }
            else
            {
                GetCart().AddItem(productName, productPrice, productEan, productSupplier, qty, Convert.ToInt16(productId));
            }
            return Redirect(redirectString);
        }

        /// <summary>
        /// This method queries the comparison service by passing in a product in order to try and retrieve the cheapest from any available suppliers
        /// </summary>
        /// <param name="productName">the name of the product</param>
        /// <param name="productPrice">the price of the product</param>
        /// <param name="productEan">the ean of the product</param>
        /// <param name="productSupplier">the supplier of the product</param>
        /// <param name="productCategory">the category of the product</param>
        /// <returns></returns>
        public StoreProduct compareItemForCart(string productName, double productPrice, string productEan, string productSupplier, string productCategory, string productId)
        {
            StoreProduct theCheapestProduct = null;
            var client = new HttpClient { BaseAddress = new System.Uri("http://localhost:18651/") };
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromMilliseconds(10000);
            try
            {
                HttpResponseMessage response = client.GetAsync("GetCheapestProduct?ean=" + productEan + "&catName=" + productCategory + "&name=" + productName + "&price=" + productPrice + "&productSupplier=" + productSupplier + "&productId=" + productId).Result;
                // if successful display the data, otherwise log the bad response
                if (response.IsSuccessStatusCode)
                {
                    theCheapestProduct = response.Content.ReadAsAsync<StoreProduct>().Result;
                    return theCheapestProduct;
                }
                else
                    Debug.WriteLine("Received a bad response from the comparison web service.");
            }
            catch (Exception ex) { Debug.WriteLine("Error getting response from comparison web service: " + ex); }
            return theCheapestProduct;
        }

        /// <summary>
        /// This method simply removes the passed in product from the cart.
        /// </summary>
        /// <param name="myProduct">The product to remove from the cart</param>
        /// <param name="returnUrl">The initial page where the item was removed from</param>
        /// <returns>Takes the user to the cart.</returns>
        public RedirectToRouteResult RemoveFromCart(string productName, string productEan, string productSupplier, string returnUrl)
        {
            GetCart().RemoveItem(productName, productEan, productSupplier);
            return RedirectToAction("Index", new { returnUrl });
        }

        #region Methods relating to placing orders

        /// <summary>
        /// Takes user to the PlaceOrder View providing they are logged in.
        /// </summary>
        /// <returns>the view for placing an order</returns>
        [Authorize(Roles = "User")]
        public ActionResult PlaceOrder()
        {
            return View();
        }

        /// <summary>
        /// Executes order requests by sending the orders to their relevant supplier
        /// </summary>
        /// <param name="theOrder"></param>
        /// <returns></returns>
        [System.Web.Http.HttpPut]
        public ActionResult ExecuteOrder(Order theOrder)
        {
            // Obtain the current sessions cart
            var theCart = GetCart();
            // For each item in the cart send the order to its specific supplier
            var orders = new List<OrderSupplierViewModel>();
            foreach (CartItem item in theCart.Items)
            {
                var details = new OrderSupplierViewModel();
                if (item.ProductSupplier == "DavisonCorp")
                {
                    details.TheOrder = SendDavisonOrderToService(User.Identity.Name, theOrder.CardNumber, item.ProductName, item.ProductEan, item.ProductPrice, item.Quantity);
                    details.ProductSupplier = item.ProductSupplier;
                    orders.Add(details);
                }
                else if (item.ProductSupplier == "Undercutters")
                {
                    details.TheOrder = SendUndercuttersOrderToService(User.Identity.Name, theOrder.CardNumber, item.ProductId, item.Quantity);
                    details.ProductSupplier = item.ProductSupplier;
                    orders.Add(details);
                }
                else if (item.ProductSupplier == "BazzasBazaar")
                {
                    details.TheOrder = SendBazzasBazaarOrderToService(User.Identity.Name, theOrder.CardNumber, item.ProductId, item.Quantity);
                    details.ProductSupplier = item.ProductSupplier;
                    orders.Add(details);
                }
                else if (item.ProductSupplier == "DodgyDealers")
                {
                    details.TheOrder = SendDodgyDealersOrderToService(User.Identity.Name, theOrder.CardNumber, item.ProductId, item.Quantity);
                    details.ProductSupplier = item.ProductSupplier;
                    orders.Add(details);
                }
            }
            // Now that the orders have been processed we empty the cart.
            theCart.RemoveAllItems();
            // Take customer to order confirmation page and display list of their orders
            return View("OrderConfirmation", orders);
        }

        private Order SendDavisonOrderToService(string accountName, string cardNumber, string productName, string productEan, double price, int quantity)
        {
            // Object created to store the obtained order information from the service
            var retrievedOrderInformation = new Order();
            // Set up connection to the web service.
            var client = new HttpClient { BaseAddress = new System.Uri("http://localhost:49623/") };
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromMilliseconds(10000);
            // Attempt to send the order to undercutters and retrieve the returned data
            try
            {
                var response = client.GetAsync("GetOrderDetailsFromDavison?AccountName=" + accountName + "&CardNumber=" + cardNumber + "&ProductName=" + productName + "&ProductEan=" + productEan + "&Price=" + price + "&Quantity=" + quantity).Result;
                // if successful return the data, otherwise log the bad response
                if (response.IsSuccessStatusCode)
                {
                    retrievedOrderInformation = response.Content.ReadAsAsync<Order>().Result;
                    return retrievedOrderInformation;
                }
                else Debug.WriteLine(DateTime.Now + "Received a bad response from the Orders web service. Response: " + response.StatusCode);
            }
            catch (Exception ex) { Debug.WriteLine("Error getting response from Orders web service: " + ex); }
            return retrievedOrderInformation;
        }

        /// <summary>
        /// Method to send an order to the Orders Service (for Undercutters).
        /// </summary>
        /// <param name="accountName">the accountname for the user making the order</param>
        /// <param name="cardNumber">the customers card number</param>
        /// <param name="productId">the id of the product being ordered</param>
        /// <param name="quantity">the quantity of the product being ordered</param>
        /// <returns>order object populated with returned order details</returns>
        public Order SendUndercuttersOrderToService(string accountName, string cardNumber, int productId, int quantity)
        {
            // Object created to store the obtained order information from the service
            var retrievedOrderInformation = new Order();
            // Set up connection to the web service.
            var client = new HttpClient { BaseAddress = new System.Uri("http://localhost:49623/") };
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromMilliseconds(20000);
            // Attempt to send the order to undercutters and retrieve the returned data
            try
            {
                var response = client.GetAsync("GetOrderDetailsFromUndercutters?AccountName=" + accountName + "&CardNumber=" + cardNumber + "&ProductId=" + productId + "&Quantity=" + quantity).Result;
                // if successful return the data, otherwise log the bad response
                if (response.IsSuccessStatusCode)   
                {
                    retrievedOrderInformation = response.Content.ReadAsAsync<Order>().Result;
                    return retrievedOrderInformation;
                }
                else Debug.WriteLine(DateTime.Now + "Received a bad response from the Orders web service. Response: " + response.StatusCode);
            }
            catch (Exception ex) { Debug.WriteLine("Error getting response from Orders web service: " + ex); }
            return retrievedOrderInformation;
        }

        /// <summary>
        /// Method to send an order to the Orders Service (for DodgyDealers).
        /// </summary>
        /// <param name="accountName">the accountname for the user making the order</param>
        /// <param name="cardNumber">the customers card number</param>
        /// <param name="productId">the id of the product being ordered</param>
        /// <param name="quantity">the quantity of the product being ordered</param>
        /// <returns>order object populated with returned order details</returns>
        public Order SendDodgyDealersOrderToService(string accountName, string cardNumber, int productId, int quantity)
        {
            // Object created to store the obtained order information from the service
            var retrievedOrderInformation = new Order();
            // Set up connection to the web service.
            var client = new HttpClient { BaseAddress = new System.Uri("http://localhost:49623/") };
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromMilliseconds(10000);
            // Attempt to send the order to undercutters and retrieve the returned data
            try
            {
                var response = client.GetAsync("GetOrderDetailsFromUndercutters?AccountName=" + accountName + "&CardNumber=" + cardNumber + "&ProductId=" + productId + "&Quantity=" + quantity).Result;
                // if successful return the data, otherwise log the bad response
                if (response.IsSuccessStatusCode)
                {
                    retrievedOrderInformation = response.Content.ReadAsAsync<Order>().Result;
                    return retrievedOrderInformation;
                }
                else Debug.WriteLine(DateTime.Now + "Received a bad response from the Orders web service. Response: " + response.StatusCode);
            }
            catch (Exception ex) { Debug.WriteLine("Error getting response from Orders web service: " + ex); }
            return retrievedOrderInformation;
        }

        /// <summary>
        /// Method to send an order to the Orders Service (for BazzasBazaar).
        /// </summary>
        /// <param name="accountName">the accountname for the user making the order</param>
        /// <param name="cardNumber">the customers card number</param>
        /// <param name="productId">the id of the product being ordered</param>
        /// <param name="quantity">the quantity of the product being ordered</param>
        /// <returns>order object populated with returned order details</returns>
        public Order SendBazzasBazaarOrderToService(string accountName, string cardNumber, int productId, int quantity)
        {
            // Object created to store the obtained order information from the service
            var retrievedOrderInformation = new Order();
            // Set up connection to the web service.
            var client = new HttpClient { BaseAddress = new System.Uri("http://localhost:49623/") };
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromMilliseconds(10000);
            // Attempt to send the order to BazzasBazaar and retrieve the returned data
            try
            {
                var response = client.GetAsync("GetOrderDetailsFromBazzas?AccountName=" + accountName + "&CardNumber=" + cardNumber + "&ProductId=" + productId + "&Quantity=" + quantity).Result;
                // if successful return the data, otherwise log the bad response
                if (response.IsSuccessStatusCode)
                {
                    retrievedOrderInformation = response.Content.ReadAsAsync<Order>().Result;
                    return retrievedOrderInformation;
                }
                else Debug.WriteLine(DateTime.Now + "Received a bad response from the Orders web service. Response: " + response.StatusCode);
            }
            catch (Exception ex) { Debug.WriteLine("Error getting response from Orders web service: " + ex); }
            return retrievedOrderInformation;
        }

        #endregion

    }
}