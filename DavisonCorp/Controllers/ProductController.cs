﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using DavisonCorpModel.Repository;
using DavisonCorpModel;
using DavisonSocial;


namespace DavisonCorp.Controllers
{
    /// <summary>
    /// Controller for Product{s} and the actions relating to them.
    /// </summary>
    public class ProductController : Controller
    {
        private readonly ProductRepository _myRepository = new ProductRepository(new Davison.Model.Context());

        /// <summary>
        /// Obtains the active product categories as stated in the Davison DB
        /// </summary>
        public ActionResult Index()
        {
            // Attempt to clear session data
            try
            {
                Session.Remove("minPrice");
                Session.Remove("maxPrice");
            }
            catch { Debug.WriteLine("Unable to remove session data for minPrice / maxPrice"); }
            // Returns the active categories from the Davison Model
            return View(_myRepository.GetDavisonCategories());
        }

        /// <summary>
        /// Shows the FilteredByCategory Parent View which shows products from suppliers (as partial views)
        /// </summary>
        /// <param name="categoryName">the name of the category selected</param>
        /// <returns>view of products in the category</returns>
        public ActionResult FilteredByCategory(string categoryName)
        {
            Session["categoryName"] = categoryName; // Store selected category in session data for later actions          
            return View();
        }

        /// <summary>
        /// Obtains Davison Products and displays them in a partial view.
        /// </summary>
        public PartialViewResult DavisonProducts()
        {
            string category = Session["categoryName"].ToString();
            double minPrice = Convert.ToDouble(Session["minPrice"]);
            double maxPrice = Convert.ToDouble(Session["maxPrice"]);
            IEnumerable<StoreProduct> data;
            if((minPrice != 0.0) && (maxPrice != 0.00))
                data = _myRepository.GetDavisonProductsByPrice(category, minPrice, maxPrice);
            else
                data = _myRepository.GetDavisonProducts(category);
            return PartialView("_PartialProducts", data);
        }

        /// <summary>
        /// Obtains UnderCutters Products and displays them in a partial view.
        /// </summary>
        public PartialViewResult UnderCutters()
        {
            string category = Session["categoryName"].ToString();
            double minPrice = Convert.ToDouble(Session["minPrice"]);
            double maxPrice = Convert.ToDouble(Session["maxPrice"]);
            IEnumerable<StoreProduct> data;
            if ((minPrice != 0.0) && (maxPrice != 0.00))
                data = _myRepository.GetUndercuttersProductsByPrice(category, minPrice, maxPrice);
            else
                data = _myRepository.GetUndercuttersProducts(category);
            return PartialView("_PartialProducts", data);
        }

        /// <summary>
        /// Obtains Bazzas Products and displays them in a partial view.
        /// </summary>
        public PartialViewResult BazzasBazaar()
        {
            string category = Session["categoryName"].ToString();
            double minPrice = Convert.ToDouble(Session["minPrice"]);
            double maxPrice = Convert.ToDouble(Session["maxPrice"]);
            IEnumerable<StoreProduct> data;
            if ((minPrice != 0.0) && (maxPrice != 0.00))
                data = _myRepository.GetBazzasProductsByCategoryAndPrice(category, minPrice, maxPrice);
            else
                data = _myRepository.GetBazzasProductsByCategory(category);
            return PartialView("_PartialProducts", data);
        }

        /// <summary>
        /// Obtains DodgyDealers Products and displays them in a partial view.
        /// </summary>
        public PartialViewResult DodgyDealers()
        {
            //create timeout for task - make 4 seconds.
            var tokenSource = new CancellationTokenSource();
            CancellationToken token = tokenSource.Token;
            const int timeOut = 2000;

            string category = Session["categoryName"].ToString();
            double minPrice = Convert.ToDouble(Session["minPrice"]);
            double maxPrice = Convert.ToDouble(Session["maxPrice"]);
            IEnumerable<StoreProduct> data = null;
            if ((minPrice != 0.0) && (maxPrice != 0.00))
            {
                var task = Task.Factory.StartNew(() => _myRepository.GetDodgyDealersProductsByPrice(category, minPrice, maxPrice), token);
                //when timeout is reached stop task or get result.
                if (task.Wait(timeOut, token))
                        data = task.Result;
            }
            else
            {
                var task = Task.Factory.StartNew(() => _myRepository.GetDodgyDealersProducts(category), token);
                if (task.Wait(timeOut, token))
                    data = task.Result;
            }

            return PartialView("_PartialProducts", data);
        }

        /// <summary>
        /// Hit when filtration options have been specified
        /// Updates the root/parent view (FilteredByCategory)
        /// </summary>
        /// <param name="minPrice">minimum product price to display</param>
        /// <param name="maxPrice">maximum product price to display</param>
        /// <returns>Take user back to the FilteredByCategory View</returns>
        public ActionResult UpdateRootView(string minPrice, string maxPrice)
        {
            string category = Session["categoryName"].ToString();
            Session["minPrice"] = minPrice;
            Session["maxPrice"] = maxPrice;
            return RedirectToAction("FilteredByCategory", new { categoryName = category } );
        }

        public ActionResult ProductDetails(string ean)
        {
            ViewBag.eanToShow = ean;
            return View();
        }
    }
}
