﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DavisonOrdersService.Controllers;

namespace DavisonOrdersService.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            OrdersHomeController controller = new OrdersHomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Home Page", result.ViewBag.Title);
        }
    }
}
