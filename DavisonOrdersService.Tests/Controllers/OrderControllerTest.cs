﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DavisonCorpModel.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DavisonOrdersService.Controllers;
using DavisonOrdersService.Models;

namespace DavisonOrdersService.Tests.Controllers
{
    /// <summary>
    /// This classs contains tests for the Order Controller from the DavisonOrdersService
    /// </summary>
    [TestClass]
    public class OrderControllerTest
    {
        /// <summary>
        /// Tests if orders can successfully be added to the OrderLog table in the Orders DB
        /// </summary>
        [TestMethod]
        public void TestAddingOrdersToDb()
        {
            var theOrders = new StoreOrders();
            
            // Mock record to add to OrdersDB
            const string supplier = "DavisonCorp";
            const int orderId = 9999;
            const string productName = "TestProduct";
            const int quantity = 20;
            const string accountName = "TestAccount@TestAccount.com";

            // Attempt to add the mock record to the database
            theOrders.OrdersInDb.Add(new DavisonOrdersService.Models.OrderLog(supplier, orderId, productName, quantity, accountName));

            // set passResult to true if the record can be found within the orders database
            var returnedItem = theOrders.OrdersInDb.Where(p => p.SupplierOrderId == orderId);

            Assert.IsNotNull(returnedItem);
        }
        
        /// <summary>
        /// This method tests if orders can successfully be placed with Undercutters
        /// It checks that an Order object is returned and populated with data.
        /// </summary>
        [TestMethod]
        public void TestCreatingUndercuttersOrder()
        {
            // Mock the data required for testing the method
            const string accountName = "TestAccount@TestAccount.com";
            const string cardNumber = "1234123412341234";
            const int productId = 1;
            const int quantity = 1;

            // Make an instance of the order controller
            var anOrderController = new OrderController();

            // store the output of the method in to a variable to check against.
            var methodOutput = anOrderController.SendOrderToUndercutters(accountName, cardNumber, productId, quantity);

            // FIRST TEST CONDITION - Ensure the returned object is of type 'Order'
            Order theOrder = new Order();
            bool indicator = methodOutput.GetType().IsInstanceOfType(theOrder);
            Assert.IsTrue(indicator);

            // SECOND TEST CONDITION - Ensure an order id is returned (by checking its not null and > 0)
            int id = methodOutput.Id;
            Assert.IsNotNull(id);
            Assert.IsTrue(id > 0);
        }

    }
}
