﻿namespace DavisonCorpModel
{
    public class OrderSupplierViewModel
    {

        public DavisonCorpModel.DTO.Order TheOrder { get; set; }

        public string ProductSupplier { get; set; }

    }
}