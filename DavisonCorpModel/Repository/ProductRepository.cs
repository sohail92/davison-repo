﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;

namespace DavisonCorpModel.Repository
{
    public class ProductRepository
    {

        public Davison.Model.Context context;

        /// <summary>
        /// Overloaded constructor that sets the db context
        /// </summary>
        /// <param name="context"></param>
        public ProductRepository(Davison.Model.Context context)
        {
            this.context = context;
        }

        /// <summary>
        /// Returns the possible product categories as dictated by the Davison.Model
        /// </summary>
        /// <returns>A list of categories obtained</returns>
        public IEnumerable<Davison.Model.Category> GetDavisonCategories()
        {
            // Display products from the Davison DB which have been set as active.
            return context.Categories.Where(p => p.Active == true);
        }

        /// <summary>
        /// Gets data from the Davison Database
        /// </summary>
        /// <returns>A list of products obtained</returns>
        public IEnumerable<StoreProduct> GetDavisonProducts(string category)
        {
            var myProducts = new List<StoreProduct>();
            foreach (var theProduct in context.Products)
            {
                bool IsProductInStock = theProduct.StockLevel > 0; // if product stock level is more then 0 set it as true
                var newProduct = new StoreProduct
                {// converts the Davison.Model.Product to the Custom product (StoreEntities.Product).
                    Id = theProduct.Id,
                    Ean = theProduct.Ean,
                    CategoryId = theProduct.CategoryId,
                    CategoryName = theProduct.Category.Name,
                    BrandId = theProduct.BrandId,
                    BrandName = theProduct.Brand.Name,
                    Name = theProduct.Name,
                    Description = theProduct.Description,
                    Price = theProduct.Price,
                    PriceForTen = theProduct.Price * 10,
                    InStock = IsProductInStock,
                    ExpectedRestock = DateTime.Now,
                    Active = theProduct.Active,
                    ProductSupplier = "DavisonCorp"
                };
                myProducts.Add(newProduct);
            }
            return myProducts.Where(p => (p.Active == true) && (p.CategoryName == category));
        }

        /// <summary>
        /// Used to filter products retrieved from the Davison Products DB by price
        /// </summary>
        /// <param name="category">the category of items to filter</param>
        /// <param name="minPrice">minimum product price to allow</param>
        /// <param name="maxPrice">maximum product price to allow</param>
        /// <returns>A list of products filtered by price</returns>
        public IEnumerable<StoreProduct> GetDavisonProductsByPrice(string category, double minPrice, double maxPrice)
        {
            return GetDavisonProducts(category).Where(p => p.Price > minPrice && p.Price < maxPrice && p.Price > minPrice);
        }

        /// <summary>
        /// Gets data from the Undercutters HTTP Web Service
        /// </summary>
        /// <returns>A list of products obtained</returns>
        public IEnumerable<StoreProduct> GetUndercuttersProducts(string category)
        {
            var client = new HttpClient { BaseAddress = new System.Uri("http://undercutters.azurewebsites.net/") };
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromMilliseconds(10000);
            try
            {
                HttpResponseMessage response = client.GetAsync("api/Product").Result;
                if (response.IsSuccessStatusCode) // if successful display the data, otherwise log the bad response
                {
                    var products = response.Content.ReadAsAsync<IEnumerable<DavisonCorpModel.DTO.Product>>().Result;
                    var myProducts = new List<StoreProduct>();
                    foreach (var theProduct in products)
                    {
                        // if product stock level is more then 0 set it as true
                        var newProduct = new StoreProduct
                        {
                            Id = theProduct.Id,
                            Ean = theProduct.Ean,
                            CategoryId = theProduct.CategoryId,
                            CategoryName = theProduct.CategoryName,
                            BrandId = theProduct.BrandId,
                            BrandName = theProduct.BrandName,
                            Name = theProduct.Name,
                            Description = theProduct.Description,
                            Price = theProduct.Price,
                            PriceForTen = theProduct.Price * 10,
                            InStock = theProduct.InStock,
                            ExpectedRestock = Convert.ToDateTime(theProduct.ExpectedRestock),
                            Active = true,
                            ProductSupplier = "Undercutters",
                        };
                        myProducts.Add(newProduct);
                    }
                    return myProducts.Where(p => p.CategoryName == category);
                }
                else Debug.WriteLine(DateTime.Now + "Error obtaining products from Undercutters. Response code: " + response.StatusCode);
            }
            catch (Exception ex) { Debug.WriteLine("Error getting response from Undercutters: " + ex); }
            return null;
        }

        /// <summary>
        /// Used to filter products retrieved from the Undercutters Web Service by price
        /// </summary>
        /// <param name="category">the category of items to filter</param>
        /// <param name="minPrice">minimum product price to allow</param>
        /// <param name="maxPrice">maximum product price to allow</param>
        /// <returns>A list of products filtered by price</returns>
        public IEnumerable<StoreProduct> GetUndercuttersProductsByPrice(string category, double minPrice, double maxPrice)
        {
            return GetUndercuttersProducts(category).Where(p => p.Price > minPrice && p.Price < maxPrice && p.Price > minPrice);
        }

        /// <summary>
        /// Gets data from the Dodgy Dealers HTTP Web Service
        /// </summary>
        /// <returns>A list of products obtained</returns>
        public IEnumerable<StoreProduct> GetDodgyDealersProducts(string category)
        {
            var client = new HttpClient { BaseAddress = new System.Uri("http://dodgydealers.azurewebsites.net/") };
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            client.Timeout = TimeSpan.FromMilliseconds(20000);
            try
            {
                for (int i = 0; i < 3; i++) // Try to get product data from dodgy dealers (up to 3 times).
                {
                    HttpResponseMessage response = client.GetAsync("api/Product").Result;
                    if (response.IsSuccessStatusCode)// if successful display the data, otherwise log the bad response
                    {
                        var products = response.Content.ReadAsAsync<IEnumerable<DavisonCorpModel.DTO.Product>>().Result;
                        var myProducts = new List<StoreProduct>();
                        foreach (var theProduct in products)
                        {
                            // if product stock level is more then 0 set it as true
                            var newProduct = new StoreProduct()
                            {
                                Id = theProduct.Id,
                                Ean = theProduct.Ean,
                                CategoryId = theProduct.CategoryId,
                                CategoryName = theProduct.CategoryName,
                                BrandId = theProduct.BrandId,
                                BrandName = theProduct.BrandName,
                                Name = theProduct.Name,
                                Description = theProduct.Description,
                                Price = theProduct.Price,
                                PriceForTen = theProduct.Price * 10,
                                InStock = theProduct.InStock,
                                ExpectedRestock = Convert.ToDateTime(theProduct.ExpectedRestock),
                                Active = true,
                                ProductSupplier = "DodgyDealers"
                            };
                            myProducts.Add(newProduct);
                        }
                        return myProducts.Where(p => p.CategoryName == category);
                    }
                    else
                        Debug.WriteLine(DateTime.Now + "Error obtaining products from Dodgy Dealers. Response code: " + response.StatusCode);
                }
            }
            catch (Exception ex) { Debug.WriteLine("Error getting response from dodgy dealers: " + ex); }
            return null;
        }

        /// <summary>
        /// Used to filter products retrieved from the DodgyDealers Web Service by price
        /// </summary>
        /// <param name="category">the category of items to filter</param>
        /// <param name="minPrice">minimum product price to allow</param>
        /// <param name="maxPrice">maximum product price to allow</param>
        /// <returns>A list of products filtered by price</returns>
        public IEnumerable<StoreProduct> GetDodgyDealersProductsByPrice(string category, double minPrice, double maxPrice)
        {
            return GetDodgyDealersProducts(category).Where(p => p.Price > minPrice && p.Price < maxPrice && p.Price > minPrice);
        }

        /// <summary>
        /// Displays products from BazzasBazaar filtered by Category
        /// </summary>
        /// <param name="categoryName">the name of the category to filter products by</param>
        /// <returns>A list of filtered products</returns>
        public IEnumerable<StoreProduct> GetBazzasProductsByCategory(string categoryName)
        {
            // Object of type StoreClient from BazzasBazaar WCF Service.
            var bb = new BazzasBazaar.StoreClient();
            // List of Products to populate from BazzasBazaar.
            var obtainedProducts = new List<StoreProduct>();
            // Variable to store the ID of the passed in category name.
            int categoryId = 0;
            // Iterate through all possible categories until the passed in one is found and set the categoryId to its Id.
            foreach (var category in bb.GetAllCategories())
                if (category.Name == categoryName)
                    categoryId = bb.GetCategoryById(category.Id).Id;
            //  Go through each product obtained & set the attributes as required by the StoreEntities Product Definition
            foreach (var theProduct in bb.GetFilteredProducts(categoryId, categoryName, null, null))
            {
                var newProduct = new StoreProduct()
                {
                    Id = theProduct.Id,
                    Ean = theProduct.Ean,
                    CategoryId = theProduct.CategoryId,
                    CategoryName = theProduct.CategoryName,
                    BrandId = 999,
                    BrandName = "Bazzas",
                    Name = theProduct.Name,
                    Description = theProduct.Description,
                    Price = theProduct.PriceForOne,
                    PriceForTen = theProduct.PriceForTen,
                    InStock = theProduct.InStock,
                    ExpectedRestock = Convert.ToDateTime(theProduct.ExpectedRestock),
                    Active = true,
                    ProductSupplier = "BazzasBazaar"
                };
                obtainedProducts.Add(newProduct);
            }
            return obtainedProducts;
        }

        /// <summary>
        /// Used to filter products retrieved from Bazzas WCF Service by price
        /// </summary>
        /// <param name="category">the category of items to filter</param>
        /// <param name="minPrice">minimum product price to allow</param>
        /// <param name="maxPrice">maximum product price to allow</param>
        /// <returns>A list of products filtered by price</returns>
        public IEnumerable<StoreProduct> GetBazzasProductsByCategoryAndPrice(string category, double minPrice, double maxPrice)
        {
            return GetBazzasProductsByCategory(category).Where(p => p.Price > minPrice && p.Price < maxPrice && p.Price > minPrice);
        }

    }
}