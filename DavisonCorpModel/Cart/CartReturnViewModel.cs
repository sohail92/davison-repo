﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DavisonCorpModel.Cart
{
    /// <summary>
    /// This view model acts as an entity for the relationship 
    /// of the cart to returning to an initial product selection page.
    /// </summary>
    public class CartReturnViewModel
    {
        /// <summary>
        /// This is the Cart object for the current users sesssion.
        /// </summary>
        public Cart cart { get; set; }
        
        /// <summary>
        /// This is the url for the product page which the customer originated from 
        /// </summary>
        public string returnUrl { get; set; }
    }
}