﻿using System.Collections.Generic;
using System.Linq;

namespace DavisonCorpModel.Cart
{
    public class Cart
    {
        private List<CartItem> ShoppingCartItems = new List<CartItem>();

        /// <summary>
        /// Adds item to cart, takes a product and quantity.
        /// </summary>
        public void AddItem(string productName, double productPrice, string productEan, string productSupplier, int quantity, int productId)
        {
            //gets item from itemCollection (current cart) where ean number equals passed through ean number (ie the same product is in the cart).
            CartItem item = ShoppingCartItems.FirstOrDefault(p => p.ProductEan == productEan && p.ProductSupplier == productSupplier);
            //if item doesnt exsist in cart add item
            if (item == null)
            {
                ShoppingCartItems.Add(new CartItem { ProductName = productName, ProductPrice = productPrice, ProductEan = productEan, ProductSupplier = productSupplier, Quantity = quantity, ProductId = productId});
            }
            //if item already exsists increase quantity.
            else
            {
                item.Quantity += quantity;
            }
        }

        /// <summary>
        /// From item from cart where it is the passed through product
        /// </summary>

        public void RemoveItem(string productName, string productEan, string productSupplier)
        {
            //remove where product ean number is equal to ean num in cart.
            ShoppingCartItems.RemoveAll(l => l.ProductEan == productEan);
        }

        /// <summary>
        /// This method removes all items from the shopping cart
        /// </summary>
        public void RemoveAllItems()
        {
            ShoppingCartItems.Clear();
        }

        /// <summary>
        /// Calculates total of item * quantity for each product.
        /// </summary>
        /// <returns></returns>
        public double CalculateTotalCartValue()
        {
            return ShoppingCartItems.Sum(e => e.ProductPrice * e.Quantity);
        }

        /// <summary>
        /// Returns all items in current cart.
        /// </summary>
        public IEnumerable<CartItem> Items
        {
            get { return ShoppingCartItems; }
        } 
    }
}