﻿namespace DavisonCorpModel.Cart
{
    /// <summary>
    /// This class represents an item inside of a cart.
    /// </summary>
    public class CartItem
    {

        public string ProductName { get; set; }

        public double ProductPrice { get; set; }

        public string ProductEan { get; set; }

        public string ProductSupplier { get; set; }

        public int Quantity { get; set; }

        public int ProductId { get; set; }
    }
}