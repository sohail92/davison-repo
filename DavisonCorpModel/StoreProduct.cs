﻿using System;
using System.ComponentModel;

namespace DavisonCorpModel
{
    public class StoreProduct
    {
        [DisplayName("Product ID")]
        public int Id { get; set; }

        [DisplayName("EAN")]
        public string Ean { get; set; }

        [DisplayName("Category ID")]
        public int CategoryId { get; set; }

        [DisplayName("Category Name")]
        public string CategoryName { get; set; }

        [DisplayName("Brand ID")]
        public int BrandId { get; set; }

        [DisplayName("Brand Name")]
        public string BrandName { get; set; }

        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Price")]
        public double Price { get; set; }

        [DisplayName("Price For Ten")]
        public double PriceForTen { get; set; }

        [DisplayName("Stock")]
        public bool InStock { get; set; }

        [DisplayName("Expected Availability")]
        public DateTime ExpectedRestock { get; set; }

        public bool Active { get; set; }

        [DisplayName("Supplier")]
        public string ProductSupplier { get; set; }
    }
}