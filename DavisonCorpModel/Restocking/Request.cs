﻿using System;

namespace DavisonCorpModel.Restocking
{
    public class Request
    {
        public int Id { get; set; }
        
        public DateTime ExpectedDeliveryDate { get; set; }
        
        public string ProductEan { get; set; }
        
        public int Quantity { get; set; }
    }
}