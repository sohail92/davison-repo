﻿using System;

namespace DavisonCorpModel.Restocking
{
    public class Delivery
    {
        public int Id { get; set; }
        
        public DateTime ArrivalDate { get; set; }
        
        public string ProductEan { get; set; }
        
        public int Quantity { get; set; }
    }
}