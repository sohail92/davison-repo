﻿namespace DavisonCorpModel.DTO
{
    public class OrderLog
    {
        public int Id { get; set; }

        public string SupplierName { get; set; }

        public int SupplierOrderId { get; set; }

        public string ProductName { get; set; }

        public int QuantityOrdered { get; set; }

        public string AccountName { get; set; }
    }
}