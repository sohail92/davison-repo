﻿namespace DavisonCorpModel.DTO
{
    public class Brand
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int AvailableProductCount { get; set; }
    }
}