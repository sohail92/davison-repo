﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using Davison.Model;
using DavisonCorpModel;
using DavisonCorpModel.Repository;

namespace DavisonComparisonService.Controllers
{
    /// <summary>
    /// Controller for comparing product prices against the available suppliers
    /// </summary>
    public class CompareController : ApiController
    {
        //creates an instance of the repository for getting products.
        private ProductRepository repo = new ProductRepository(new Context());


        /// <summary>
        /// Used to return a list of the cheapest products from available sources.
        /// </summary>
        /// <param name="itemsToCompare">a list of items that will be compared to find the cheapest</param>
        /// <returns>a list of cheapest items</returns>
        [HttpGet]
        [Route("GetCheapestProducts")]
        public IHttpActionResult GetCheapestproducts(IEnumerable<StoreProduct> itemsToCompare)
        {
            //used to store the cheapest items when found.
            var cheapestItems = new List<StoreProduct>();
            //for every product passed through compare in each source to find cheaper items.
            foreach (var item in itemsToCompare)
            {
                var cheapestitem = item;
                if (item.ProductSupplier != "DavisonCorp")
                    cheapestitem = CompareAgainstDavison(item);
                if (item.ProductSupplier != "Undercutters")
                    cheapestitem = CompareAgainstUnderCutters(item);
                if (item.ProductSupplier != "BazzasBazaar")
                    cheapestitem = CompareAgainstBazzas(item);
                if (item.ProductSupplier != "DodgyDealers")
                    cheapestitem = CompareAgainstDodgyDealers(item);
                cheapestItems.Add(cheapestitem);
            }
            return Ok(cheapestItems);
        }

        /// <summary>
        /// used to return a cheaper product if available.
        /// </summary>
        /// <param name="itemToCompare">the item to compare</param>
        /// <returns>the cheapest item found</returns>
        [HttpGet]
        [System.Web.Http.Route("GetCheapestProduct")]
        public IHttpActionResult GetCheapestproduct(string ean, string catName, string name, double price, string productSupplier, string productId)
        {
            //converts the parameters passed through into a store product.
            var itemToCompare = SetProduct(ean, catName,name, price, productSupplier, productId);
            // Stores the cheapest item found. Initialises cheapest item to one passed through incase no cheaper can be found.
            var cheapestitem = itemToCompare;
            //create timeout for task - make 4 seconds.
            var tokenSource = new CancellationTokenSource();
            CancellationToken token = tokenSource.Token;
            const int timeOut = 2000;
            //compares item against all sources.
            if (cheapestitem.ProductSupplier != "DavisonCorp")
            {
                //sets item to compare.
                var compare = cheapestitem;
                //starts a task for comparing against source and passes through timeout token.
                var task = Task.Factory.StartNew(() => CompareAgainstDavison(compare), token);
                //when timeout is reached stop task or get result.
                if (task.Wait(timeOut, token))
                {
                    if (task.Result != null)
                    {
                        cheapestitem = task.Result;
                    }
                }

                else
                    Debug.WriteLine("Davison ran out of time");
            }
            if (cheapestitem.ProductSupplier != "Undercutters")
            {
                StoreProduct compare = cheapestitem;
                var task = Task.Factory.StartNew(() => CompareAgainstUnderCutters(compare), token);
                if (task.Wait(timeOut, token))
                {
                    if (task.Result != null)
                    {
                        cheapestitem = task.Result;
                    }
                }
                else
                    Debug.WriteLine("Undercutters ran out of time");
            }
            if (cheapestitem.ProductSupplier != "BazzasBazaar")
            {
                var compare = cheapestitem;
                var task = Task.Factory.StartNew(() => CompareAgainstBazzas(compare), token);
                if (task.Wait(timeOut, token))
                {
                    if (task.Result != null)
                    {
                        cheapestitem = task.Result;
                    }
                }
                else
                    Debug.WriteLine("Bazzas ran out of time");
            }
            if (cheapestitem.ProductSupplier != "DodgyDealers")
            {
                var compare = cheapestitem;
                var task = Task.Factory.StartNew(() => CompareAgainstDodgyDealers(compare), token);
                if (task.Wait(timeOut, token))
                {
                    if (task.Result != null)
                    {
                        cheapestitem = task.Result;
                    }
                }
                else
                    Debug.WriteLine("DodgyDealers ran out of time");
            }
            return Ok(cheapestitem); //returns the cheapest item found.
        }

        /// <summary>
        /// Sets a passed in products details to create a StoreProduct object as used in the store
        /// </summary>
        public virtual StoreProduct SetProduct(string ean, string catName, string name, double price, string productSupplier, string productId)
        {
            var product = new StoreProduct()
            {
                Ean = ean,
                CategoryName = catName,
                Name = name,
                Price = price,
                ProductSupplier = productSupplier,
                Id = Convert.ToInt32(productId),
            };
            return product;
        }

        /// <summary>
        /// Compares passed in item to any item in the davison store with the same ean and where the product is active and in stock. 
        /// </summary>
        /// <param name="itemToCompare"></param>
        /// <returns>a store product which will either be the original item or the cheaper item from the cource if found.</returns>
        public virtual StoreProduct CompareAgainstDavison(StoreProduct itemToCompare)
        {
            var cheapestItem = itemToCompare;
            try
            {
                //get products from passed in category.
                var davisonProducts = repo.GetDavisonProducts(cheapestItem.CategoryName);
                //for each item compare against parameters and check if item is cheaper.
                foreach (var productFromDavison in davisonProducts)
                {
                    if (itemToCompare.Ean == productFromDavison.Ean)
                    {
                        if (itemToCompare.Price > productFromDavison.Price)
                        {
                            if (productFromDavison.Active)
                            {
                                if (productFromDavison.InStock)
                                {
                                    //if item is cheaper and available set to the new cheapest item to be passed on for the next compare.
                                    cheapestItem = productFromDavison;
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
                Debug.WriteLine("Problem comparing davison products");
                return cheapestItem;
            }
            return cheapestItem;
        }

        /// <summary>
        /// Compares passed in item to any item in bazzas bazzar with the same ean and where the product is active and in stock. 
        /// </summary>
        public virtual StoreProduct CompareAgainstBazzas(StoreProduct itemToCompare)
        {
            var cheapestItem = itemToCompare;
            try
            {
                var bazzasProducts = repo.GetBazzasProductsByCategory(cheapestItem.CategoryName);
                foreach (var bazzasProduct in bazzasProducts)
                {
                    if (itemToCompare.Ean == bazzasProduct.Ean)
                    {
                        if (itemToCompare.Price > bazzasProduct.Price)
                        {
                            if (bazzasProduct.Active)
                            {
                                if (bazzasProduct.InStock)
                                {
                                    cheapestItem = bazzasProduct;
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
                Debug.WriteLine("Problem comparing Bazzas products");
                return cheapestItem;
            }
            return cheapestItem;
        }

        /// <summary>
        /// Compares passed in item to any item in Undercutters with the same ean and where the product is active and in stock. 
        /// </summary>
        public virtual StoreProduct CompareAgainstUnderCutters(StoreProduct itemToCompare)
        {
            var cheapestItem = itemToCompare;
            try
            {
                var undercuttersProducts = repo.GetUndercuttersProducts(cheapestItem.CategoryName);
                foreach (var undercuttersProduct in undercuttersProducts)
                {
                    if (itemToCompare.Ean == undercuttersProduct.Ean)
                    {
                        if (itemToCompare.Price > undercuttersProduct.Price)
                        {
                            if (undercuttersProduct.Active)
                            {
                                if (undercuttersProduct.InStock)
                                {
                                    cheapestItem = undercuttersProduct;
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
                Debug.WriteLine("Problem comparing UnderCutters products");
                return cheapestItem;
            }
            return cheapestItem;
        }

        /// <summary>
        /// Compares passed in item to any item to the dodgy dealers source with the same ean and where the product is active and in stock. 
        /// </summary>
        public virtual StoreProduct CompareAgainstDodgyDealers(StoreProduct itemToCompare)
        {
            var cheapestItem = itemToCompare;
            try
            {
                var dodgydealersProducts = repo.GetDodgyDealersProducts(cheapestItem.CategoryName);
                foreach (StoreProduct dodgydealerproduct in dodgydealersProducts)
                {
                    if (itemToCompare.Ean == dodgydealerproduct.Ean)
                    {
                        if (itemToCompare.Price > dodgydealerproduct.Price)
                        {
                            if (dodgydealerproduct.Active)
                            {
                                if (dodgydealerproduct.InStock)
                                {
                                    cheapestItem = dodgydealerproduct;
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
                Debug.WriteLine("Problem comparing Dodgy Dealers products");
                return cheapestItem;
            }
            return cheapestItem;
        }

    }
}
