﻿using System.Web.Mvc;

namespace DavisonComparisonService.Controllers
{
    public class CompareHomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            return View();
        }
    }
}
